import { useAuthContext } from "utility/Essentials/Authentication";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { EventsAPI } from "api/EventsAPI";

// Import the AccountsAPI to use API backend custom function calls
import AccountsAPI from "api/AccountsAPI";

import "../../../styles/index.css";

function HTML(props) {
  const { volunteers, handleDelete, volunteerNames } = props;

  return (
    <div className="flex justify-center max-w-md mx-auto ">
      <table className="table-auto border-separate border-spacing-2 rounded-xl shadow-xl shadow-beige shadow-slate-300 shadow w-90 border border-slate-300  mt-4 ...">
        <thead>
          <tr>
            <th className="h-8 w-full text-sm pl-2 bg-tansparent outline-blue text-center">
              Name of Volunteers
            </th>
            <th className="h-8 w-full text-sm pl-2 bg-tansparent outline-blue text-center">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
          {volunteerNames?.map((volunteer, index) => (
            // [0 Isaac] [1 John]
            <tr key={index}>
              <td className="font-medium uppercase text-brown  h-8 w-full rounded-md border border-brown text-sm pl-2 bg-tansparent outline-blue text-center">
                {volunteer}
              </td>

              <td className="inline-block rounded bg-beige-light px-7 pt-3 pb-2.5 text-sm font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700">
                <button onClick={() => handleDelete(volunteers[index])}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

function VolunteersHandler() {
  const [volunteerNames, setVolunteerNames] = useState([]);
  const [volunteers, setVolunteers] = useState([]);
  const { token } = useAuthContext();
  const { event_id } = useParams();

  async function handleDelete(volunteer_id) {
    console.log("aaaaa", volunteer_id);
    try {
      await EventsAPI.removeVolunteer(event_id, volunteer_id, token);
      const filteredVolunteers = volunteerNames.filter(
        (volunteer) => volunteer.id !== volunteer_id
      );

      setVolunteerNames(filteredVolunteers);
    } catch (err) {}
  }

  async function getVolunteer() {
    try {
      const data = await EventsAPI.getVolunteers(event_id, token);
      console.log("Volunteers", data);
      setVolunteers(data);
    } catch (err) {
      console.log("ERROR FETCHING VOLUNTEERS", err);
    }
  }

  async function fetchVolunteersDetails() {
    // Returns an array of volunteer names
    const volunteersDetails = await Promise.all(
      volunteers.map(async (volunteer) => {
        const volunteerDetail = await AccountsAPI.GetVolunteerDetail(volunteer);
        return volunteerDetail.first_name;
      })
    );
    console.log("Volunteers Names", volunteersDetails);
    setVolunteerNames(volunteersDetails);
  }
  // [1,2]
  // [Isaac, John]


  return {
    fetchVolunteersDetails,
    handleDelete,
    volunteerNames,
    getVolunteer,
    volunteers,
  };
}

function ListVolunteers() {
  const {
    handleDelete,
    fetchVolunteersDetails,
    volunteerNames,
    getVolunteer,
    volunteers,
  } = VolunteersHandler();

  useEffect(() => {
    getVolunteer();
  }, []);

  useEffect(() => {
    fetchVolunteersDetails();
  }, [volunteers]);

  return (
    <HTML
      volunteers={volunteers}
      handleDelete={handleDelete}
      volunteerNames={volunteerNames}
    />
  );
}

export default ListVolunteers;
