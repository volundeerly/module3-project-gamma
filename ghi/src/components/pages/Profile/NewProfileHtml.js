import ProfileImage from "images/ProfileImage.png";
import ModalButtonHtml from "../Misc/ModalButtonHtml";

// Top Banner and Name
function AccountHeaderCard({ ProfileData }) {
  return (
    <div className="bounce-hover brown-border my-1 theme-background-1  shadow-xl pb-8">
      <div className="w-full ">
        <img
          src="https://vojislavd.com/ta-template-demo/assets/img/profile-background.jpg"
          className="z-[-2] w-full h-full"
        />
      </div>

      <div className="flex flex-col items-center -mt-20">
        <div className="flex items-center justify-center h-24 w-24 border-4 border-[#78350f] rounded-full overflow-hidden">
          <img
            className="h-full w-full object-cover"
            src={ProfileImage}
            alt="Profile Picture"
          />
        </div>

        <div className="flex items-center space-x-2 mt-2">
          <p className="volundeerly-font text-2xl">{`${ProfileData.first_name} ${ProfileData.last_name}`}</p>
          <span
            className="bg-blue-500 rounded-full p-1"
            title="Verified"
          ></span>
        </div>
        <p className="volundeerly-font text-gray-700">A Volunteer.</p>
        <p className="text-sm text-gray-500"></p>
      </div>
      <div className="flex-1 flex flex-col items-center lg:items-end justify-end px-8 mt-2">
        <div className="flex items-center space-x-4 mt-2"></div>
      </div>
    </div>
  );
}

function AccountActivityLog(props) {
  return (
    <div className="bounce-hover brown-border theme-background-1 rounded-lg shadow-xl mt-1 p-8">
      <h4 className="volundeerly-font text-xl text-gray-900 font-bold">
        Activity log
      </h4>
      <div className="px-4"></div>
    </div>
  );
}

function AccountInformation({ ProfileData }) {
  return (
    <div className="my-4 mt-4 2xl:w-1/3">
      <div className="bounce-hover flex-1 brown-border theme-background-1 rounded-lg shadow-xl p-8">
        <h4 className="volundeerly-font text-xl text-gray-900 font-bold">
          Personal Info
        </h4>
        <ul className="mt-2 text-gray-700">
          <li className="flex border-bottom-black border-top-black dark:border-black-600 py-2">
            <span className="volundeerly-font font-bold w-24">Full name:</span>
            <span className="volundeerly-font text-gray-700">{`${ProfileData.first_name} ${ProfileData.last_name}`}</span>
          </li>

          {<li className="flex border-bottom-black dark:border-black-600 py-2">
            <span className="volundeerly-font font-bold w-24">Birthday:</span>
            <span className="volundeerly-font text-gray-700">------</span>
          </li> ? (
            ProfileData.birthday !== null
          ) : null}

          {<li className="flex border-bottom-black dark:border-black-600 py-2">
            <span className="font-bold w-24">Joined:</span>
            <span className="text-gray-700">------</span>
          </li> ? (
            ProfileData.joined !== null
          ) : null}

          <li className="flex border-bottom-black dark:border-black-600 py-2">
            <span className="volundeerly-font font-bold w-24">Mobile:</span>
            <span className="volundeerly-font text-gray-700">{`${ProfileData.phone_num}`}</span>
          </li>

          <li className="flex border-bottom-black dark:border-black-600 py-2">
            <span className="volundeerly-font font-bold w-24">Username:</span>
            <span className="volundeerly-font text-gray-700">{`${ProfileData.username}`}</span>
          </li>

          <li className="flex border-bottom-black dark:border-black-600 py-2">
            <span className="volundeerly-font font-bold w-24">Email:</span>
            <span className="volundeerly-font text-gray-700">{`${ProfileData.email}`}</span>
          </li>

          <li className="flex border-bottom-black dark:border-black-600 py-2">
            <span className="volundeerly-font font-bold w-24">Zip Code:</span>
            <span className="volundeerly-font text-gray-700">{`${ProfileData.zip_code}`}</span>
          </li>

          {<li className="flex items-center border-b py-2 space-x-2">
            <span className="volundeerly-font font-bold w-24">Elsewhere:</span>
            <a href="#" title="Facebook"></a>
            <a href="#" title="Twitter"></a>
            <a href="#" title="LinkedIn"></a>
            <a href="#" title="Github"></a>
          </li> ? (
            ProfileData.links !== null
          ) : null}
        </ul>
      </div>
      <div className="bounce-hover flex-1 brown-border theme-background-1 rounded-lg shadow-xl mt-4 p-8">
        <h4 className="volundeerly-font text-xl text-gray-900 font-bold">
          About
        </h4>
        <p className="volundeerly-font mt-2 text-gray-700">
          {`${ProfileData.bio}` ? ProfileData.bio !== null : ""}
        </p>
      </div>
    </div>
  );
}
//w-full flex flex-col
function CardsInformation(props) {
  let {
    IsLoading,
    ProfileData,
    accountevents,
    handleRemoveVolunteer,
    token,
    showConfirmModal,
    setShowConfirmModal,
    eventToDelete,
    setEventToDelete,
    ConfirmDeleteModal,
    createdevents,
  } = props.Props;

  function handleShowConfirmModal(event) {
    setEventToDelete(event);
    setShowConfirmModal(true);
  }

  function handleCloseConfirmModal() {
    setShowConfirmModal(false);
    setEventToDelete(null);
  }

  function handleConfirmDelete() {
    if (eventToDelete) {
      handleRemoveVolunteer(eventToDelete.id, token);
      handleCloseConfirmModal();
    }
  }

  return (
    <div className="2xl:w-2/3">
      <div className="bounce-hover brown-border theme-background-1 rounded-lg shadow-xl mt-4 p-8">
        {/*=======*/}
        <div className="right-profile-grid justify-center text-center">
          <div className="py-8 max-w-screen-2xl mx-auto flex-column text-center events-wrapperz">
            <h2 className="text-3xl font-bold mb-4 text-center">
              Volunteering At:
            </h2>
            <div
              className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4"
              id="volunteer_at"
            >
              {accountevents.map((accountevent) => (
                <div
                  key={accountevent.id}
                  className="bg-beige-light rounded-lg shadow-md p-4 relative"
                >
                  <button
                    className="absolute right-2 top-2 text-red-500"
                    onClick={() => handleShowConfirmModal(accountevent)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                  </button>
                  <h3 className="text-xl font-bold mb-2">
                    {accountevent.name}
                  </h3>
                  <p className="text-gray-600 mb-2">
                    {new Date(accountevent.start_date).toLocaleDateString()} -{" "}
                    {new Date(accountevent.end_date).toLocaleDateString()}
                  </p>
                  <p className="text-gray-800 mb-4">
                    {accountevent.description}
                  </p>
                  <div className="flex justify-end mt-4">
                    <ModalButtonHtml
                      type={"button"}
                      name={"learnmoreprofile-button"}
                      size={"small"}
                      text={"Learn more"}
                      onClick={() => {
                        window.location.href = `/events/detail/${accountevent.id}`;
                      }}
                      classes={"button-size-2"}
                    />
                  </div>
                </div>
              ))}
            </div>
            <ConfirmDeleteModal
              show={showConfirmModal}
              onClose={handleCloseConfirmModal}
              onConfirm={handleConfirmDelete}
            />
            <div className="py-8 max-w-screen-2xl mx-auto">
              <h2 className="text-3xl font-bold mb-4" id="created_events">
                Created Events:
              </h2>
              <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                {createdevents.map((createdevent) => (
                  <div
                    key={createdevent.id}
                    className="bg-beige-light rounded-lg shadow-md p-4 relative"
                  >
                    <h3 className="text-xl font-bold mb-2">
                      {createdevent.name}
                    </h3>
                    <p className="text-gray-600 mb-2">
                      {new Date(createdevent.start_date).toLocaleDateString()} -{" "}
                      {new Date(createdevent.end_date).toLocaleDateString()}
                    </p>
                    <p className="text-gray-800 mb-4">
                      {createdevent.description}
                    </p>
                    <div className="flex justify-end mt-4">
                      <ModalButtonHtml
                        type={"button"}
                        name={"editevent-button"}
                        size={"small"}
                        text={"Edit event"}
                        onClick={() => {
                          window.location.href = `/events/${createdevent.id}`;
                        }}
                        classes={"button-size-2"}
                      />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        {/*=======*/}
        <h4 className="hidden text-xl text-gray-900 font-bold">Statistics</h4>

        <div className="hidden grid grid-cols-1 lg:grid-cols-3 gap-8 mt-4">
          <div className="px-6 py-6 bg-gray-100 border border-gray-300 rounded-lg shadow-xl">
            <div className="flex items-center justify-between">
              <span className="font-bold text-sm text-indigo-600">
                Total Revenue
              </span>
              <span className="text-xs bg-gray-200 hover:bg-gray-500 text-gray-500 hover:text-gray-200 px-2 py-1 rounded-lg transition duration-200 cursor-default">
                7 days
              </span>
            </div>
            <div className="flex items-center justify-between mt-6">
              <div></div>
              <div className="flex flex-col">
                <div className="flex items-end">
                  <span className="text-2xl 2xl:text-3xl font-bold">
                    $8,141
                  </span>
                  <div className="flex items-center ml-2 mb-1">
                    <span className="font-bold text-sm text-gray-500 ml-0.5">
                      3%
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="px-6 py-6 bg-gray-100 border border-gray-300 rounded-lg shadow-xl">
            <div className="flex items-center justify-between">
              <span className="font-bold text-sm text-green-600">
                New Orders
              </span>
              <span className="text-xs bg-gray-200 hover:bg-gray-500 text-gray-500 hover:text-gray-200 px-2 py-1 rounded-lg transition duration-200 cursor-default">
                7 days
              </span>
            </div>
            <div className="flex items-center justify-between mt-6">
              <div></div>
              <div className="flex flex-col">
                <div className="flex items-end">
                  <span className="text-2xl 2xl:text-3xl font-bold">217</span>
                  <div className="flex items-center ml-2 mb-1">
                    <span className="font-bold text-sm text-gray-500 ml-0.5">
                      5%
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="px-6 py-6 bg-gray-100 border border-gray-300 rounded-lg shadow-xl">
            <div className="flex items-center justify-between">
              <span className="font-bold text-sm text-blue-600">
                New Connections
              </span>
              <span className="text-xs bg-gray-200 hover:bg-gray-500 text-gray-500 hover:text-gray-200 px-2 py-1 rounded-lg transition duration-200 cursor-default">
                7 days
              </span>
            </div>
            <div className="flex items-center justify-between mt-6">
              <div></div>
              <div className="flex flex-col">
                <div className="flex items-end">
                  <span className="text-2xl 2xl:text-3xl font-bold">54</span>
                  <div className="flex items-center ml-2 mb-1">
                    <span className="font-bold text-sm text-gray-500 ml-0.5">
                      7%
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-4">
          <canvas
            id="verticalBarChart"
            style={{
              display: "block",
              boxSizing: "border-box",
              height: "414px",
              width: "828px",
            }}
            width="1656"
            height="828"
          ></canvas>
        </div>
      </div>
    </div>
  );
}

// Connections
function ConnectionsInformation(props) {
  return (
    <div className="bg-white rounded-lg shadow-xl p-8">
      <div className="flex items-center justify-between">
        <h4 className="text-xl text-gray-900 font-bold">Connections (532)</h4>
        <a href="#" title="View All"></a>
      </div>
      <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 2xl:grid-cols-8 gap-8 mt-8">
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection1.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Diane Aguilar</p>
          <p className="text-xs text-gray-500 text-center">
            UI/UX Design at Upwork
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection2.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Frances Mather</p>
          <p className="text-xs text-gray-500 text-center">
            Software Engineer at Facebook
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection3.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Carlos Friedrich</p>
          <p className="text-xs text-gray-500 text-center">
            Front-End Developer at Tailwind CSS
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection4.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Donna Serrano</p>
          <p className="text-xs text-gray-500 text-center">
            System Engineer at Tesla
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection5.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Randall Tabron</p>
          <p className="text-xs text-gray-500 text-center">
            Software Developer at Upwork
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection6.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">John McCleary</p>
          <p className="text-xs text-gray-500 text-center">
            Software Engineer at Laravel
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection7.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Amanda Noble</p>
          <p className="text-xs text-gray-500 text-center">
            Graphic Designer at Tailwind CSS
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection8.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Christine Drew</p>
          <p className="text-xs text-gray-500 text-center">
            Senior Android Developer at Google
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection9.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Lucas Bell</p>
          <p className="text-xs text-gray-500 text-center">
            Creative Writer at Upwork
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection10.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Debra Herring</p>
          <p className="text-xs text-gray-500 text-center">
            Co-Founder at Alpine.js
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection11.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Benjamin Farrior</p>
          <p className="text-xs text-gray-500 text-center">
            Software Engineer Lead at Microsoft
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection12.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Maria Heal</p>
          <p className="text-xs text-gray-500 text-center">
            Linux System Administrator at Twitter
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection13.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Edward Ice</p>
          <p className="text-xs text-gray-500 text-center">
            Customer Support at Instagram
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection14.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Jeffery Silver</p>
          <p className="text-xs text-gray-500 text-center">
            Software Engineer at Twitter
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection15.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Jennifer Schultz</p>
          <p className="text-xs text-gray-500 text-center">
            Project Manager at Google
          </p>
        </a>
        <a
          href="#"
          className="flex flex-col items-center justify-center text-gray-800 hover:text-blue-600"
          title="View Profile"
        >
          <img
            src="https://vojislavd.com/ta-template-demo/assets/img/connections/connection16.jpg"
            className="w-16 rounded-full"
          />
          <p className="text-center font-bold text-sm mt-1">Joseph Marlatt</p>
          <p className="text-xs text-gray-500 text-center">
            Team Lead at Facebook
          </p>
        </a>
      </div>
    </div>
  );
}

function NewProfileHtml(props) {
  // IsLoading: Boolean true or false.
  // ProfileData: Dictionary containing details for the account.
  let {
    IsLoading,
    ProfileData,
    accountevents,
    handleRemoveVolunteer,
    token,
    showConfirmModal,
    setShowConfirmModal,
    eventToDelete,
    setEventToDelete,
    ConfirmDeleteModal,
    createdevents,
  } = props;

  // If IsLoading is true, return a Loading HTML page.
  if (IsLoading) return <div className="App">Loading...</div>;

  // Turn ProfileData Dicitionary into a mapable array.

  return (
    <div className="m-4 grid grid-cols-5 sm:grid-cols-5 md:grid-cols-5 lg:grid-cols-5 grid-rows-1 gap-4">
      <div className="col-span-2 sm:col-span-2 md:col-span-2 lg:col-span-2 row-span-1">
        <AccountHeaderCard ProfileData={ProfileData} />
        <AccountInformation ProfileData={ProfileData} />
      </div>
      <div className="col-span-3 sm:col-span-3 md:col-span-3 lg:col-span-3 row-span-1">
        <AccountActivityLog />
        <CardsInformation Props={props} />
      </div>
    </div>
  );
}

//     <div className="h-full p-8">

//      <ProfileMenuPart3 />

/** Export the component function */
export default NewProfileHtml;
