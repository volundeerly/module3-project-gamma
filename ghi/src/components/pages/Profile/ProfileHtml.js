/* ======================================================== */
/* Main Component Function: */

/** Micro Component to hold all the JSX and HTML code for the component. */
function ProfileHtml(props) {
  // IsLoading: Boolean true or false.
  // ProfileData: Dictionary containing details for the account.
  let {
    IsLoading,
    ProfileData,
    accountevents,
    handleRemoveVolunteer,
    token,
    showConfirmModal,
    setShowConfirmModal,
    eventToDelete,
    setEventToDelete,
    ConfirmDeleteModal,
    createdevents,
  } = props;

  // If IsLoading is true, return a Loading HTML page.
  if (IsLoading) return <div className="App">Loading...</div>;

  // Turn ProfileData Dicitionary into a mapable array.
  function handleShowConfirmModal(event) {
    setEventToDelete(event);
    setShowConfirmModal(true);
  }

  function handleCloseConfirmModal() {
    setShowConfirmModal(false);
    setEventToDelete(null);
  }

  function handleConfirmDelete() {
    if (eventToDelete) {
      handleRemoveVolunteer(eventToDelete.id, token);
      handleCloseConfirmModal();
    }
  }
  // Return the Profile Data HTML page.
  return (
    <>
      <div className="page-wrapper">
        <div className="grid-profile grid grid-cols-2 gap-10">
          <div className="card-client" id="cc">
            <div className="user-picture">
              <svg viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg">
                <path d="M224 256c70.7 0 128-57.31 128-128s-57.3-128-128-128C153.3 0 96 57.31 96 128S153.3 256 224 256zM274.7 304H173.3C77.61 304 0 381.6 0 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7C432.5 512 448 496.5 448 477.3C448 381.6 370.4 304 274.7 304z"></path>
              </svg>
            </div>
            <p className="name-client" id="nameclient">
              {" "}
              {`${ProfileData.first_name} ${ProfileData.last_name}`}
              {/* <span className="social-media" id="socialmedia">
                <a href="/profile/me">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                  </svg>
                  <span className="tooltip-social">Twitter</span>
                </a>
                <a href="/profile/me">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                  </svg>
                  <span className="tooltip-social">Instagram</span>
                </a>
                <a href="/profile/me">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"></path>
                  </svg>
                  <span className="tooltip-social">Facebook</span>
                </a>
                <a href="/profile/me">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
                  </svg>
                  <span className="tooltip-social">LinkedIn</span>
                </a>
              </span> */}
              <span>Bio: {ProfileData.bio}</span>
              <span>CE: {ProfileData.created_events.length}</span>
              <span>JE: {ProfileData.joined_events.length}</span>
            </p>
          </div>
          <div className="right-profile-grid justify-center text-center">
            <div className="py-8 max-w-screen-2xl mx-auto flex-column text-center events-wrapperz">
              <h2 className="text-3xl font-bold mb-4 text-center">
                Volunteering At:
              </h2>
              <div
                className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4"
                id="volunteer_at"
              >
                {accountevents.map((accountevent) => (
                  <div
                    key={accountevent.id}
                    className="bg-beige-light rounded-lg shadow-md p-4 relative"
                  >
                    <button
                      className="absolute right-2 top-2 text-red-500"
                      onClick={() => handleShowConfirmModal(accountevent)}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                          d="M6 18L18 6M6 6l12 12"
                        />
                      </svg>
                    </button>
                    <h3 className="text-xl font-bold mb-2 mt-2">
                      {accountevent.name}
                    </h3>
                    <p className="text-gray-600 mb-2">
                      {new Date(accountevent.start_date).toLocaleDateString()} -{" "}
                      {new Date(accountevent.end_date).toLocaleDateString()}
                    </p>
                    <p className="text-gray-800 mb-4">
                      {accountevent.description}
                    </p>
                    <div className="flex justify-end mt-4">
                      <a
                        href={`/events/detail/${accountevent.id}`}
                        className="text-indigo-500 font-bold"
                      >
                        Learn more &rarr;
                      </a>
                    </div>
                  </div>
                ))}
              </div>
              <ConfirmDeleteModal
                show={showConfirmModal}
                onClose={handleCloseConfirmModal}
                onConfirm={handleConfirmDelete}
              />
              <div className="py-8 max-w-screen-2xl mx-auto">
                <h2 className="text-3xl font-bold mb-4" id="created_events">
                  Created Events:
                </h2>
                <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                  {createdevents.map((createdevent) => (
                    <div
                      key={createdevent.id}
                      className="bg-beige-light rounded-lg shadow-md p-4 relative"
                    >
                      <h3 className="text-xl font-bold mb-2">
                        {createdevent.name}
                      </h3>
                      <p className="text-gray-600 mb-2">
                        {new Date(createdevent.start_date).toLocaleDateString()}{" "}
                        - {new Date(createdevent.end_date).toLocaleDateString()}
                      </p>
                      <p className="text-gray-800 mb-4">
                        {createdevent.description}
                      </p>
                      <div className="flex justify-end mt-4">
                        <a
                          href={`/events/${createdevent.id}`}
                          className="text-indigo-500 font-bold"
                        >
                          Edit event &rarr;
                        </a>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

/** Export the component function */
export default ProfileHtml;
/* ======================================================== */
