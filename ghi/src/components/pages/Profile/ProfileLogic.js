/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import React, { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";

// Import the useToken authenticator for authenticating login.
import { useAuthContext, useAuth } from "utility/Essentials/Authentication";
// HTML Component for the Signup Form
import ProfileHtml from "components/pages/Profile/ProfileHtml";
import NewProfileHtml from "components/pages/Profile/NewProfileHtml";
// Import the AccountsAPI to use API backend custom function calls
import AccountsAPI from "api/AccountsAPI";
import { EventsAPI } from "api/EventsAPI";

import * as Helpers from "utility/Misc/Helpers";

import { useParams } from "react-router-dom";

/* ======================================================== */
/* Micro Component Functions: */

/** Micro Component to hold the states and assosciating functions for the Login Form Data */
function ProfileHandler() {
  // ======================================= //
  const navigate = useNavigate();
  const [profileData, setProfileData] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const { account_param } = useParams();
  // ======================================= //

  async function GetProfileData(token, account_id) {
    if (account_id && account_param === account_id.toString()) {
      navigate("/profile/me");
      return;
    }

    // If its null, this is not a valid Url and page
    if (account_param == null) {
      return;
    }
    // If account_id is "me", means this is the users current profile.
    else if (account_param === "me") {
      if (!token || !account_id) {
        console.log("NOT LOGGED IN");
        return;
      }
      let ResponseData = await AccountsAPI.GetProfileData(account_id);
      setProfileData(ResponseData);
      setLoading(false);
    }
    // The account_id is a valid number which coordinates to a specific account.
    else if (Helpers.isNumeric(account_param)) {
      let ResponseData = await AccountsAPI.GetProfileData(account_param);
      setProfileData(ResponseData);
      setLoading(false);
    }
  }
  // ======================================= //

  return { isLoading, profileData, GetProfileData };
}

function MyEventsHandler() {
  const [accountevents, setAccountEvents] = useState([]);
  const [createdevents, setCreatedEvents] = useState([]);

  async function GetAccountEvents(token) {
    try {
      const data = await EventsAPI.getAccountEvents(token);
      setAccountEvents(data);
    } catch (err) {
      console.log("ERROR FETCHING ACCOUTNS EVENTS", err);
    }
  }

  async function GetCreatedEvents(token) {
    try {
      const data = await EventsAPI.getCreatedEvents(token);
      setCreatedEvents(data);
    } catch (err) {
      console.log("ERROR FETCHING ACCOUTNS EVENTS", err);
    }
  }

  async function handleRemoveVolunteer(event_id, token) {
    try {
      await EventsAPI.removeVolunteer(event_id, token);
      setAccountEvents((prevEvents) =>
        prevEvents.filter((event) => event.id !== event_id)
      );
    } catch (err) {
      console.log("ERROR DELETING EVENT", err);
    }
  }

  return {
    GetAccountEvents,
    handleRemoveVolunteer,
    accountevents,
    createdevents,
    GetCreatedEvents,
  };
}

function ConfirmDeleteModal({ show, onClose, onConfirm }) {
  if (!show) {
    return null;
  }

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center">
      <div className="absolute inset-0 bg-black opacity-50"></div>
      <div className="bg-white w-96 rounded-lg shadow-lg z-10">
        <div className="p-4">
          <h3 className="text-lg font-bold mb-2">Confirm Witdraw</h3>
          <p className="text-gray-700 mb-4">
            Are you sure you want to withdraw from this event?
          </p>
          <div className="flex justify-end">
            <button
              className="bg-gray-300 hover:bg-gray-400 rounded-lg px-4 py-2 mr-4"
              onClick={onClose}
            >
              Cancel
            </button>
            <button
              className="bg-red-500 hover:bg-red-600 rounded-lg px-4 py-2 text-white"
              onClick={onConfirm}
            >
              Withdraw
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
/* ======================================================== */
/* Main Component Functions:
 */

function ProfileLogic(props) {
  // Get Tokens or Account_Id
  const { token, account_id } = useAuthContext();
  // Call the ProfileHandler Micro Component and return the State altering Functions and Data for use in the JSX
  const { isLoading, profileData, GetProfileData } = ProfileHandler();
  const {
    GetAccountEvents,
    handleRemoveVolunteer,
    accountevents,
    GetCreatedEvents,
    createdevents,
  } = MyEventsHandler();
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [eventToDelete, setEventToDelete] = useState(null);

  // ======================================= //
  useEffect(() => {
    GetAccountEvents(token);
    GetCreatedEvents(token);
    GetProfileData(token, account_id);
  }, [token, account_id]);

  // ======================================= //

  Helpers.ToggleScrollLock("scroll");
  // Return the HTML/JSX Data
  return (
    // Call the HTML Micro Component and Pass in the Function & Data as Props.
    <NewProfileHtml
      IsLoading={isLoading}
      ProfileData={profileData}
      accountevents={accountevents}
      createdevents={createdevents}
      handleRemoveVolunteer={handleRemoveVolunteer}
      token={token}
      showConfirmModal={showConfirmModal}
      setShowConfirmModal={setShowConfirmModal}
      eventToDelete={eventToDelete}
      setEventToDelete={setEventToDelete}
      ConfirmDeleteModal={ConfirmDeleteModal}
    />
  );
}
/*   <ProfileHtml
      IsLoading={isLoading}
      ProfileData={profileData}
      accountevents={accountevents}
      createdevents={createdevents}
      handleRemoveVolunteer={handleRemoveVolunteer}
      token={token}
      showConfirmModal={showConfirmModal}
      setShowConfirmModal={setShowConfirmModal}
      eventToDelete={eventToDelete}
      setEventToDelete={setEventToDelete}
      ConfirmDeleteModal={ConfirmDeleteModal}
    />
    */
/** Export the component function */
export default ProfileLogic;
/* ======================================================== */
