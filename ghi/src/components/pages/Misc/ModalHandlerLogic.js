/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import { useState } from "react";
// Helper Functions
import { ToggleScrollLock, IsEscapeKey } from "utility/Misc/Helpers";

/* ======================================================== */
/* Micro Component Functions: */

function ModalHandlerLogic() {
  // Sets a State for whether the Modal is Shown or not Shown
  const [isShown, setIsShown] = useState(false);
  const [isOpacity, setIsOpacity] = useState(false);
  const [isTransform, setIsTransform] = useState(false);
  const [isScale, setIsScale] = useState(false);

  /* ============================================= */

  // Function when a Button is Clicked
  function ButtonClick(event) {
    event.stopPropagation();

    // Close Modal if the right button was pressed
    if (
      event.currentTarget.id === "close-button" ||
      event.target.id === "close-button"
    ) {
      CloseModal();
    }
  }

  // Function which Shows the Modal
  function ShowModal() {
    setIsShown(true);
    ToggleScrollLock("locked");
    setTimeout(() => {
      setIsOpacity(true);
      setIsTransform(true);
      setIsScale(true);
    }, 100);
  }

  // Function which Closes the Modal
  function CloseModal() {
    //LogoutButtonHtml.focus();
    setIsTransform(false);
    ToggleScrollLock("locked");

    setTimeout(() => {
      setIsOpacity(false);
    }, 100);

    setTimeout(() => {
      setIsShown(false);
      setIsScale(false);
    }, 300);
  }

  // Function which handles Key presses
  function OnKeyDown(event) {
    if (IsEscapeKey(event)) {
      CloseModal();
    }
  }

  // Function when you click outside of the Modal to close it
  function OnClickOutside(event) {
    if (event.target.id === "modal-container") {
      CloseModal();
    }
  }

  /* ============================================= */

  // Returns needed Functions
  return {
    isScale,
    isOpacity,
    isTransform,
    isShown,
    CloseModal,
    ButtonClick,
    ShowModal,
    OnKeyDown,
    OnClickOutside,
  };
}

/** Export the component function */
export default ModalHandlerLogic;
/* ======================================================== */
