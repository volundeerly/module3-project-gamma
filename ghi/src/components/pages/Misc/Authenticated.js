/* ======================================================== */
/* Imports: */
import { Navigate } from "react-router-dom";
import { useAuthContext } from "utility/Essentials/Authentication";
import React, { useEffect } from "react";

/* ======================================================== */
/* Micro Component Functions: */

function Authenticated({ children }) {
  /* ============================================= */
  // Get the Token from the useAuthContext in the Autenthenticator.
  const { token } = useAuthContext();
  /* ============================================= */
  // Redirect to Home if the Route is protected, if not, show the child component.

  useEffect(() => {}, [token]);

  if (token == null) {
    return <Navigate to="/" replace />;
  }

  return children;
  /* ============================================= */
}

/** Export the component function */
export default Authenticated;
/* ======================================================== */
