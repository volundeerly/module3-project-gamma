import React from "react";
import { animated, useSpring } from "react-spring";
import DeerGif from "images/deer.gif";

function Home() {
  const fadeIn = useSpring({
    from: { opacity: 0 },
    to: { opacity: 1 },
    delay: 300,
    config: { duration: 1000 },
  });

  const slideIn = useSpring({
    from: { transform: "translate3d(-100%, 0, 0)" },
    to: { transform: "translate3d(0, 0, 0)" },
    delay: 300,
    config: { duration: 1000 },
  });

  return (
    <>
      <section className="p-10">
        <div className="home-section flex flex-col items-center">
          <animated.div
            className="flex flex-col items-center justify-center mb-12 lg:mb-20"
            style={fadeIn}
          >
            <img
              src={DeerGif}
              className="w-64 md:w-96 lg:w-auto rounded-full"
              alt="Home_gif"
            />
          </animated.div>
          <animated.div className="text-center px-4 lg:px-0" style={slideIn}>
            <h2 className="text-brown text-2xl md:text-3xl font-semibold mb-4">
              A Communtiy That Loves To Lend A Hand!
            </h2>
            <p className="text-green text-lg md:text-xl">
              Join us today to volunteer for an event or find the volunteers you
              need to get the job done!
            </p>
          </animated.div>
        </div>
      </section>
    </>
  );
}

export default Home;
