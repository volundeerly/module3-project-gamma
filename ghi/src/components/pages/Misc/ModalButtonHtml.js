/* ======================================================== */
/* Imports: */

/* Import the Helper functions */
import { ButtonDisabled } from "utility/Misc/Helpers";
/* Import the useNavigate function for Links */
import { useNavigate } from "react-router-dom";
/* Import the Sound Effects */
import { default as SFX } from "utility/Importers/SoundImports";

/* ======================================================== */
/* Micro Component Functions: */

/** Micro Component to handle the SFX for Button Hovering, and passed Button Hover Events */
function ButtonHover(event, onHover) {
  event.stopPropagation();

  if (ButtonDisabled(event, "check")) {
    return;
  }

  // Check if Sound is already playing, if it is, return
  if (SFX.ArcadeTap1.playing() === true) {
  }

  // Play a sound effect
  SFX.ArcadeTap1.play();

  if (onHover) {
    onHover(event);
  }
}

/** Micro Component to handle the SFX for Button Clicking, and passed Button Click Events */
function ButtonClick(event, onClick) {
  event.stopPropagation();

  if (ButtonDisabled(event)) {
    return;
  }

  // Play a sound effect
  SFX.ButtonClicks["PlayRandomSound"]();

  if (onClick) {
    onClick(event);
  }
}

/* ======================================================== */
/* Main Component Functions: */

/**
 * Reuseable Button Component:
 * - Settings Via Props:
 * @param {string} name - the id or name to be given to the dom element
 * @param {string} linkTo - navigation link (route)
 * @param {string} classes - a string of classes to be added to the buttons
 * @param {string} size - size name to access specific css classes: "reg" or "small"
 * @param {string} type - button type: "button" or "submit"
 * @param {string} text - the text to be displayed in the button
 * @param {string} form - used for associating a form Id with the Button
 * @param {function} onClick - a function that would be fired when the button is clicked
 * @param {function} onHover - a function that would be fired when the button is hovered over
 */
function ModalButtonHtml(props) {
  // For Navigation
  const navigate = useNavigate();

  // Return the Button JSX
  return (
    <button
      form={props.form}
      type={props.type}
      className={`pushme ${props.classes} shadow-md`}
    >
      <span
        className={`${props.size ? `inner-${props.size}` : ""} ${
          props.classes
        }`}
        form={props.form}
        onClick={(event) => {
          ButtonClick(event, props.onClick);
          if (props.linkTo) {
            navigate(props.linkTo);
          }
        }}
        name={props.name}
        id={props.name}
        onMouseOver={(event) => {
          ButtonHover(event, props.onHover);
        }}
        type={props.type}
      >
        {props.text}
      </span>
    </button>
  );
}

/** Export the component function */
export default ModalButtonHtml;

/* ======================================================== */
