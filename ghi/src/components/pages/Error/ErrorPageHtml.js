/* ======================================================== */
/* Main Component Function: */

/** Micro Component to hold all the JSX and HTML code for the component. */
function ErrorPageHtml(props) {
  return (
    <div className="flex justify-center">
      <div className="py-8 flex-grow max-h-500">
        <h1 className="text-4xl font-bold mb-4 upcoming-events-heading">404</h1>
        <h3 className="text-2xl font-bold mb-4 upcoming-events-heading">
          PAGE NOT FOUND
        </h3>
        <h4 className="text-1xl font-bold mb-4 upcoming-events-heading">
          GET OFF
        </h4>
      </div>
    </div>
  );
}

/** Export the component function */
export default ErrorPageHtml;
/* ======================================================== */
