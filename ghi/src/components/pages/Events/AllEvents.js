// Import the useToken authenticator for authenticating login.
import { useAuthContext } from "utility/Essentials/Authentication";

import React, { useState, useEffect } from "react";

import { EventsAPI } from "api/EventsAPI";
import Draggable from "react-draggable";
import { ToggleScrollLock } from "utility/Misc/Helpers";
import "styles/index.css";

import ModalButtonHtml from "../Misc/ModalButtonHtml";

function ALLEVENTS(props) {
  const [filterType, setFilterType] = useState("date");
  const [modalEvent, setModalEvent] = useState(null);
  const [showModal, setShowModal] = useState(false);

  let {
    events,
    endDate,
    startDate,
    endHrs,
    startHrs,
    setStartDate,
    setEndDate,
    setEndHrs,
    setStartHrs,
    token,
    setAccountEvents,
  } = props;

  const handleLearnMoreClick = (event) => {
    setModalEvent(event);
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  const resetFilters = () => {
    setStartDate(null);
    setEndDate(null);
    setStartHrs(null);
    setEndHrs(null);
  };

  return (
    <div className="py-8 max-w-screen-2xl mx-auto">
      <div className="flex justify-center mb-10">
        <h2 className="text-3xl font-bold mb-4 text-center">Explore events</h2>
      </div>
      <div className="mb-8 flex justify-center">
        <ModalButtonHtml
          type={"button"}
          name={"reset-button"}
          size={"reg"}
          text={"Reset"}
          onClick={resetFilters}
          classes={"button-size-1"}
        />

        <div className="flex flex-col mb-2">
          <label htmlFor="filterType" className="text-gray-700 mr-2">
            Filter Between:
          </label>
          <select
            id="filterType"
            value={filterType}
            onChange={(e) => setFilterType(e.target.value)}
            className="border border-gray-400 py-1 px-2 rounded-lg"
          >
            <option value="date">Date</option>
            <option value="hours">Hours</option>
          </select>
        </div>

        {filterType === "date" && (
          <>
            <div className="flex flex-col mb-2">
              <label htmlFor="startDate" className="text-gray-700 mx-2">
                Start Date:
              </label>
              <input
                id="startDate"
                type="date"
                value={startDate}
                onChange={(e) => setStartDate(e.target.value)}
                className="border border-gray-400 py-1 px-2 rounded-lg"
              />
            </div>
            <div className="flex flex-col mb-2">
              <label htmlFor="endDate" className="text-gray-700 mx-2">
                End Date:
              </label>
              <input
                id="endDate"
                type="date"
                value={endDate}
                onChange={(e) => setEndDate(e.target.value)}
                className="border border-gray-400 py-1 px-2 rounded-lg"
              />
            </div>
          </>
        )}

        {filterType === "hours" && (
          <>
            <div className="flex flex-col mb-2">
              <label htmlFor="startHour" className="text-gray-700 mx-2">
                Min Hrs:
              </label>
              <input
                id="startHrs"
                type="number"
                value={startHrs}
                onChange={(e) => setStartHrs(e.target.value)}
                className="border border-gray-400 py-1 px-2 rounded-lg"
              />
            </div>
            <div className="flex flex-col mb-2">
              <label htmlFor="endHour" className="text-gray-700 mx-2">
                Max Hrs:
              </label>
              <input
                id="endHrs"
                type="number"
                value={endHrs}
                onChange={(e) => setEndHrs(e.target.value)}
                className="border border-gray-400 py-1 px-2 rounded-lg"
              />
            </div>
          </>
        )}
      </div>

      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 gap-4 flex text-center">
        <ModalButtonHtml
          className="add-event-button"
          name={"addevent-button"}
          size={"reg"}
          text={"Add Event"}
          onClick={() => (window.location.href = "/events/create")}
        />
        {events
          .filter((event) => {
            const eventStartDate = new Date(event.start_date);
            const eventEndDate = new Date(event.end_date);
            const eventHrs = Number(event.hrs);

            if (startDate && endDate && startHrs && endHrs) {
              return (
                eventStartDate >= new Date(startDate) &&
                eventEndDate <= new Date(endDate + "T23:59:59.999") &&
                eventHrs >= startHrs &&
                eventHrs <= endHrs
              );
            } else if (startDate && endDate) {
              return (
                eventStartDate >= new Date(startDate) &&
                eventEndDate <= new Date(endDate + "T23:59:59.999")
              );
            } else if (startHrs && endHrs) {
              return eventHrs >= startHrs && eventHrs <= endHrs;
            }

            return true;
          })
          .map((event) => (
            <div
              key={event.id}
              className="bg-beige-light rounded-lg shadow-md p-4 brown-border"
            >
              <h3 className="text-xl font-bold mb-2">{event.name}</h3>
              <p className="text-gray-600 mb-2">
                {new Date(event.start_date).toLocaleDateString()} -{" "}
                {new Date(event.end_date).toLocaleDateString()}
              </p>
              <p className="text-gray-800 mb-4">
                Description: {event.description}
              </p>
              <div className="flex justify-end mt-4">
                <ModalButtonHtml
                  type={"button"}
                  name={"learnmore-button"}
                  size={"reg"}
                  text={"Learn more"}
                  onClick={() => handleLearnMoreClick(event)}
                  classes={"button-size-1"}
                />
              </div>
            </div>
          ))}
      </div>

      {showModal && (
        <EventModal
          event={modalEvent}
          handleClose={handleModalClose}
          token={token}
          setAccountEvents={setAccountEvents}
        />
      )}
    </div>
  );
}

function UPCOMINGEVENTS(props) {
  const { accountevents } = props;
  return (
    <div className="flex flex-col items-center max-h-500 px-4 md:px-0">
      <div className="py-8 w-full">
        <h2 className="text-3xl font-bold mb-4 md:text-3xl ">
          {/* upcoming-events-heading */}
          Your Upcoming Events
        </h2>
        <div className="flex flex-col h-full justify-center">
          <div className="flex-grow max-h-500">
            <div className="py-2 align-middle inline-block min-w-full">
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Event name
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Details
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-beige-light divide-y divide-gray-200">
                    {accountevents.map((accountevent) => (
                      <tr key={accountevent.id}>
                        <td className="px-6 py-4 whitespace-nowrap">
                          {accountevent.name}
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <ModalButtonHtml
                            type={"button"}
                            name={"reset-button"}
                            size={"small"}
                            text={"Details"}
                            onClick={() => {
                              window.location.href = `/events/detail/${accountevent.id}`;
                            }}
                            classes={"button-size-2"}
                          />

                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function EventModal({ event, handleClose, token, setAccountEvents }) {
  async function joinEvent(event_id) {
    try {
      await EventsAPI.addVolunteer(event_id, token);
      setAccountEvents((prevEvents) =>
        prevEvents.filter((event) => event.id !== event_id)
      ); // remove the joined event from the account events
    } catch (error) {
      console.log("ERROR ADDING VOLUNTEER", error);
    }
  }

  return (
    <div className="modal-wrapper">
      <Draggable>
        <div className="modal-content brown-border">
          <div className="handle">☰</div>
          <h3 className="text-xl font-bold mb-2">{event.name}</h3>
          <p className="text-gray-600 mb-2">
            {new Date(event.start_date).toLocaleDateString()} -{" "}
            {new Date(event.end_date).toLocaleDateString()}
          </p>
          <p className="text-gray-800 mb-4">Description: {event.description}</p>
          <p className="text-gray-800 mb-4">Location: {event.location}</p>
          <p className="text-gray-800 mb-4">Total Hrs: {event.hrs}</p>
          <div className="flex justify-end mt-4">
            <ModalButtonHtml
              type={"button"}
              name={"joinevent-button"}
              size={"reg"}
              text={"Join Event"}
              onClick={() => joinEvent(event.id)}
              classes={"button-size-1"}
            />
            <ModalButtonHtml
              type={"button"}
              name={"close-button"}
              size={"reg"}
              text={"Close"}
              onClick={handleClose}
              classes={"button-size-1"}
            />
          </div>
        </div>
      </Draggable>
    </div>
  );
}

function FilterEvents() {
  const [endDate, setEndDate] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [startHrs, setStartHrs] = useState(null);
  const [endHrs, setEndHrs] = useState(null);

  return {
    endDate,
    startDate,
    startHrs,
    endHrs,
    setStartDate,
    setEndDate,
    setStartHrs,
    setEndHrs,
  };
}
function AllEvents() {
  const [events, setEvents] = useState([]);

  async function GetData() {
    try {
      const data = await EventsAPI.getAllEvents();
      setEvents(data);
    } catch (err) {
      console.log("ERROR FETCHING EVENTS", err);
    }
  }

  return {
    GetData,
    events,
  };
}

function UpcomingEvents() {
  const [accountevents, setAccountEvents] = useState([]);

  async function GetAccountEvents(token) {
    try {
      const data = await EventsAPI.getAccountEvents(token);
      setAccountEvents(data);
    } catch (err) {
      console.log("ERROR FETCHING ACCOUTNS EVENTS", err);
    }
  }

  return {
    GetAccountEvents,
    accountevents,
    setAccountEvents,
  };
}

function Events() {
  const { GetData, events } = AllEvents();
  const { GetAccountEvents, accountevents, setAccountEvents } =
    UpcomingEvents();
  const { token } = useAuthContext();
  const {
    endDate,
    startDate,
    endHrs,
    startHrs,
    setStartDate,
    setEndDate,
    setEndHrs,
    setStartHrs,
  } = FilterEvents();

  ToggleScrollLock("scroll");

  useEffect(() => {
    GetData();
    GetAccountEvents(token);
  }, [token]);

  ToggleScrollLock("scroll");
  return (
    <>
      <div className="max-w-screen-2xl mx-auto">
        <div className="grid-profile grid grid-cols-2 gap-10">
          <div className="allevents-wrapper">
            <ALLEVENTS
              events={events}
              endDate={endDate}
              startDate={startDate}
              endHrs={endHrs}
              startHrs={startHrs}
              setStartDate={setStartDate}
              setEndDate={setEndDate}
              setEndHrs={setEndHrs}
              setStartHrs={setStartHrs}
              token={token}
              setAccountEvents={setAccountEvents}
            />
          </div>
          <div className="flex flex-col justify-center">
            <div className="p-4 upcoming-wrapper">
              <UPCOMINGEVENTS
                accountevents={accountevents}
                setAccountEvents={setAccountEvents}
                token={token}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default Events;
