import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { EventsAPI } from "api/EventsAPI";
import "../../../styles/index.css";

function formatDateTime(dateTimeStr) {
  const dateTime = new Date(dateTimeStr);
  const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const timeOptions = { hour: "numeric", minute: "numeric" };
  const formattedDate = dateTime.toLocaleDateString(undefined, dateOptions);
  const formattedTime = dateTime.toLocaleTimeString(undefined, timeOptions);
  return `${formattedDate}  ${formattedTime}`;
}





function HTML(props) {
  let { event } = props;
  console.log("PROPS", props);

  return (
    <div className="flex justify-center max-w-md mx-auto ">
      <table className="events-list-wrapper table-auto border-separate border-spacing-2 rounded-xl shadow-xl shadow-beige shadow-slate-300 shadow w-90 border border-slate-300  mt-4 ..">
        <tbody>
          <tr>
            <td className="h-8 w-40 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md ">
              Event name:
            </td>
            <td className="h-8 w-40 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md border border-brown">
              {event.name}
            </td>
          </tr>
          <tr>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md ">
              Event start date:
            </td>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md border border-brown">
              {formatDateTime(event.start_date)}
            </td>
          </tr>
          <tr>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md ">
              Event end date:
            </td>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md border border-brown">
              {formatDateTime(event.end_date)}
            </td>
          </tr>
          <tr>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md ">
              Event volunteer hours:
            </td>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md border border-brown">
              {event.hrs}
            </td>
          </tr>
          <tr>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md ">
              Event Host:
            </td>
            <td className="h-8 w-50 text-sm pl-2 bg-tansparent outline-blue text-center font-medium uppercase text-brown rounded-md border border-brown">
              {event.account_id}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

function ListEventsHandler() {
  const [event, setEvent] = useState([]);
  const { event_id } = useParams();

  async function GetData() {
    try {
      const data = await EventsAPI.getEvent(parseInt(event_id));
      setEvent(data);
    } catch (err) {}
  }

  return {
    ListEventsHandler,
    GetData,
    event,
  };
}

function EventDetail() {
  const { GetData, event } = ListEventsHandler();

  useEffect(() => {
    GetData();
  }, []);

  return (
    <>
      <HTML event={event} />
    </>
  );
}
export default EventDetail;
