// Import the useToken authenticator for authenticating login.
import { useAuthContext } from "utility/Essentials/Authentication";

import React, { useState, useEffect } from "react";

import { EventsAPI } from "api/EventsAPI";

import { ToggleScrollLock } from "utility/Misc/Helpers";

function MYEVENTS(props) {
  let { accountevents, handleRemoveVolunteer, token } = props;
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [eventToDelete, setEventToDelete] = useState(null);

  function handleShowConfirmModal(event) {
    setEventToDelete(event);
    setShowConfirmModal(true);
  }

  function handleCloseConfirmModal() {
    setShowConfirmModal(false);
    setEventToDelete(null);
  }

  function handleConfirmDelete() {
    if (eventToDelete) {
      handleRemoveVolunteer(eventToDelete.id, token);
      handleCloseConfirmModal();
    }
  }

  return (
    <div className="py-8 max-w-screen-2xl mx-auto">
      <h2 className="text-3xl font-bold mb-4">Volunteering At:</h2>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
        {accountevents.map((accountevent) => (
          <div
            key={accountevent.id}
            className="bg-beige-light rounded-lg shadow-md p-4 relative"
          >
            <button
              className="absolute right-2 top-2 text-red-500"
              onClick={() => handleShowConfirmModal(accountevent)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
            <h3 className="text-xl font-bold mb-2">{accountevent.name}</h3>
            <p className="text-gray-600 mb-2">
              {new Date(accountevent.start_date).toLocaleDateString()} -{" "}
              {new Date(accountevent.end_date).toLocaleDateString()}
            </p>
            <p className="text-gray-800 mb-4">{accountevent.description}</p>
            <div className="flex justify-end mt-4">
              <a
                href={`/events/${accountevent.id}`}
                className="text-indigo-500 font-bold"
              >
                Learn more &rarr;
              </a>
            </div>
          </div>
        ))}
      </div>
      <ConfirmDeleteModal
        show={showConfirmModal}
        onClose={handleCloseConfirmModal}
        onConfirm={handleConfirmDelete}
      />
    </div>
  );
}

function ConfirmDeleteModal({ show, onClose, onConfirm }) {
  if (!show) {
    return null;
  }

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center">
      <div className="absolute inset-0 bg-black opacity-50"></div>
      <div className="bg-white w-96 rounded-lg shadow-lg z-10">
        <div className="p-4">
          <h3 className="text-lg font-bold mb-2">Confirm Delete</h3>
          <p className="text-gray-700 mb-4">
            Are you sure you want to delete this event?
          </p>
          <div className="flex justify-end">
            <button
              className="bg-gray-300 hover:bg-gray-400 rounded-lg px-4 py-2 mr-4"
              onClick={onClose}
            >
              Cancel
            </button>
            <button
              className="bg-red-500 hover:bg-red-600 rounded-lg px-4 py-2 text-white"
              onClick={onConfirm}
            >
              Delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

function MyEvents() {
  const [accountevents, setAccountEvents] = useState([]);

  async function GetAccountEvents(token) {
    try {
      const data = await EventsAPI.getAccountEvents(token);
      setAccountEvents(data);
    } catch (err) {
      console.log("ERROR FETCHING ACCOUTNS EVENTS", err);
    }
  }
  async function handleRemoveVolunteer(event_id, token) {
    try {
      await EventsAPI.removeVolunteer(event_id, token);
      setAccountEvents((prevEvents) =>
        prevEvents.filter((event) => event.id !== event_id)
      );
    } catch (err) {
      console.log("ERROR DELETING EVENT", err);
    }
  }

  return {
    GetAccountEvents,
    handleRemoveVolunteer,
    accountevents,
  };
}

function Events() {
  const { GetAccountEvents, handleRemoveVolunteer, accountevents } = MyEvents();
  const { token } = useAuthContext();

  useEffect(() => {
    GetAccountEvents(token);
  }, [token, GetAccountEvents]);

  ToggleScrollLock("scroll");
  return (
    <>
      <div className="max-w-screen-2xl mx-auto">
        <div className="flex justify-center">
          <div className="allevents-wrapper text-center ">
            <MYEVENTS
              accountevents={accountevents}
              handleRemoveVolunteer={handleRemoveVolunteer}
              token={token}
            />
          </div>
        </div>
      </div>
    </>
  );
}
export default Events;
