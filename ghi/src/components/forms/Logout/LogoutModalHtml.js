/* ======================================================== */
/* Main Component Function: */
import React from "react";
import { ModalButtonHtml } from "utility/Importers/PageImports";
/* ======================================================== */

function ModalHeaderHtml(props) {
  return (
    <div className="items-start justify-between p-4 border-bottom-black rounded-t dark:border-black-600">
      <h3 className="volundeerly-font relative text-xl font-semibold center-please text-brown dark:text-white">
        Logout
      </h3>
      <h5 className="volundeerly-font relative text-sm center-please text-green dark:text-white">
        You're about to be logged out, are you sure?
      </h5>
    </div>
  );
}

function ModalFooterHtml(props) {
  const { ButtonClick, HandleLogout } = props;
  return (
    <div className="center-please items-center p-5 space-x-3 rounded-b ">
      <ModalButtonHtml
        type={"button"}
        name={"logout-button"}
        text={"Oh, I'm sure."}
        classes={"button-size-1"}
        size="reg"
        onClick={HandleLogout}
      />
      <ModalButtonHtml
        type={"button"}
        classes={"button-size-1"}
        size="reg"
        name={"close-button"}
        text={"No, sorry."}
        onClick={ButtonClick}
      />
    </div>
  );
}

/* ======================================================== */

function LogoutModalHtml(props) {
  /*==========================================*/
  let {
    ButtonClick,
    ButtonHover,
    OnKeyDown,
    OnClickOutside,
    isTransform,
    isOpacity,
    isScale,
  } = props.ModalFunctions;
  let HandleLogout = props.HandleLogout;
  /*==========================================*/

  return (
    <aside
      tag="aside"
      role="dialog"
      tabIndex="-1"
      aria-modal="true"
      id="modal-container"
      className="modal-wrapper modal-section center-please"
      onClick={OnClickOutside}
      onKeyDown={OnKeyDown}
    >
      {/*===========================*/}
      <div
        id="modal-container"
        tabIndex="-1"
        aria-hidden="true"
        className="modal-content-f fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full"
      >
        <div className="bounce-hover relative h-full max-w-2xl min-w-60 md:h-auto">
          {/*<!-- Modal content -->*/}
          <div
            className={`
              ${isTransform ? "" : "-translate-y-full"}
              ${isOpacity ? "" : "opacity-0"}
              ${isScale ? "" : "scale-150"}
                "transform transition-opacity transition-transform duration-300 theme-background-1 brown-border relative bg-white rounded-lg shadow-[0_35px_35px_-15px_rgba(0,0,0,0.3)] dark:bg-gray-700`}
          >
            {/*<!-- Modal header -->*/}
            <ModalHeaderHtml />
            {/*<!-- Modal footer -->*/}
            <ModalFooterHtml
              ButtonClick={ButtonClick}
              HandleLogout={HandleLogout}
              ButtonHover={ButtonHover}
            />
          </div>
        </div>
      </div>
    </aside>
  );
}

/** Export the component function */
export default LogoutModalHtml;
/* ======================================================== */
