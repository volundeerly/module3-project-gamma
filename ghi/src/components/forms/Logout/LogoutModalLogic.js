/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import React from "react";
// Import the useToken authenticator for authenticating login.
import { useToken } from "utility/Essentials/Authentication";
// HTML Component for the Signup Form
import LogoutModalHtml from "components/forms/Logout/LogoutModalHtml";
import {
  ModalButtonHtml,
  ModalHandlerLogic,
} from "utility/Importers/PageImports";

import { useNavigate } from "react-router-dom";

/* ======================================================== */
/* Micro Component Functions: */

/** Micro Component to hold hooks and call the ModalHandler, and handle the Logout Button */
function LogoutHandler() {
  /* ============================================= */
  // Get an Object of Functions from the LogoutModalHandler
  const ModalFunctions = ModalHandlerLogic();
  // Get the Logout Function from the Authentication's useToken
  const { logout } = useToken();
  // Assign useNavigate to a variable for ease of use.
  const navigate = useNavigate();
  /* ============================================= */

  // Function which will be called when the Yes is selected to Logout the current user.
  async function HandleLogout() {
    // Call the Authentication Logout function
    let Status = await logout();
    // If Logout was succesful
    if (Status) {
      // Close the Modal
      ModalFunctions.CloseModal();
      // Change Pages to the Home Page
      navigate("/");
    }
    // Logout fails
    else {
      console.log("Logout Fail (not logged in probably)");
    }
  }

  /* ============================================= */
  // Return the ModelFunctions Object and the HandleLogout Function
  return { HandleLogout, ModalFunctions };
}

/* ======================================================== */
/* Main Component Functions: */

function LogoutModalLogic() {
  // Call the LoginHandler Micro Component and return the State altering Functions and Data for use in the JSX
  const { HandleLogout, ModalFunctions } = LogoutHandler();

  // Return the HTML/JSX Data

  return (
    <React.Fragment>
      <ModalButtonHtml
        type={"button"}
        text={"Logout"}
        classes={"button-size-1"}
        size="reg"
        onClick={ModalFunctions.ShowModal}
      />
      {ModalFunctions.isShown ? (
        <LogoutModalHtml
          ModalFunctions={ModalFunctions}
          HandleLogout={HandleLogout}
        />
      ) : null}
    </React.Fragment>
  );
}

/** Export the component function */
export default LogoutModalLogic;
/* ======================================================== */
