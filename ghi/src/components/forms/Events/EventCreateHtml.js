/* ======================================================== */
/* Main Component Function: */

/** Micro Component to hold tag related JSX and HTML code for the component */
function TagInputHtml(props) {
  let { AddTag, RemoveTag, tags } = props;
  return (
    <div>
      <form onSubmit={AddTag}>
        <input
          type="text"
          name="tagInput"
          className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
        />
        <button
          type="submit"
          className="inline-block m-3 p-4 rounded bg-beige-light px-4 pt-2 pb-2.5 text-xs font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
        >
          Add Tag
        </button>
      </form>
      <ul>
        {tags.map((tag, index) => (
          <li key={index}>
            {tag} <button onClick={() => RemoveTag(index)}>Remove</button>
          </li>
        ))}
      </ul>
    </div>
  );
}

/** Micro Component to hold all the JSX and HTML code for the component. */
function EventCreateHtml(props) {
  let { eventData, HandleSubmit, HandleEventChange } = props;

  return (
    <div>
      <div className="eventCreate-wrapper ">
        <div className="flex justify-center items-center h-screen bg-slate-200">
          <div
            id="form"
            className="block bg-slate-50 p-6 rounded-xl shadow-xl shadow-beige shadow-slate-300 shadow w-90"
          >
            <form onSubmit={HandleSubmit}>
              <h1 className="text-brown text-lg m-3 p-4 w-full text-center font-extrabold">
                Create Event
              </h1>
              <label
                htmlFor="name"
                className="block text-brown text-sm font-bold mb-2"
              >
                Event Name
                <input
                  className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  value={eventData.name}
                  name="name"
                  type="text"
                  onChange={HandleEventChange}
                />
              </label>
              <label
                htmlFor="start_date"
                className="block text-brown text-sm font-bold mb-2"
              >
                Start Date
                <input
                  className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  value={eventData.start_date}
                  name="start_date"
                  type="datetime-local"
                  onChange={HandleEventChange}
                />
              </label>
              <label
                htmlFor="end_date"
                className="block text-brown text-sm font-bold mb-2"
              >
                End Date
                <input
                  className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  value={eventData.end_date}
                  name="end_date"
                  type="datetime-local"
                  onChange={HandleEventChange}
                />
              </label>
              <label
                htmlFor="location"
                className="block text-brown text-sm font-bold mb-2"
              >
                Location
                <input
                  className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  value={eventData.location}
                  name="location"
                  type="text"
                  onChange={HandleEventChange}
                />
              </label>
              <label
                htmlFor="description"
                className="block text-brown text-sm font-bold mb-2"
              >
                Description
                <input
                  className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  value={eventData.description}
                  name="description"
                  type="text"
                  onChange={HandleEventChange}
                />
              </label>
              <label
                htmlFor="hrs"
                className="block text-brown text-sm font-bold mb-2"
              >
                Total Hours
                <input
                  className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  value={eventData.hrs}
                  name="hrs"
                  type="number"
                  onChange={HandleEventChange}
                />
              </label>

              <div className="text-brown text-lg m-3 p-2 w-full text-center font-extrabold">
                <button
                  type="submit"
                  className="inline-block rounded bg-beige-light px-7 pt-3 pb-2.5 text-sm font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                >
                  Create
                </button>
              </div>
            </form>
            <div>
              <label
                htmlFor="hrs"
                className="block text-brown text-sm font-bold mb-2"
              >
                Tags
                <TagInputHtml
                  AddTag={props.AddTag}
                  RemoveTag={props.RemoveTag}
                  tags={props.tags}
                />
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

/** Export the component function */
export default EventCreateHtml;
/* ======================================================== */
