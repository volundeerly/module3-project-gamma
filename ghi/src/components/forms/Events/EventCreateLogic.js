/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import React, { useState } from "react";
// Import the useAuthContext for authenticating login.
import { useAuthContext } from "utility/Essentials/Authentication";
// HTML Component for the Signup Form
import EventCreateHtml from "components/forms/Events/EventCreateHtml";
// Import the EventsAPI to use API backend custom function calls
import { EventsAPI } from "api/EventsAPI";

// Import custom page protector
import { useAuth } from "utility/Essentials/Authentication";

/* ======================================================== */
/* Micro Component Functions: */

function EventCreateHandler() {
  const { token, account_id } = useAuthContext();
  console.log("TOOOOO", token, account_id)
  const [eventData, setEventData] = useState({
    name: "",
    start_date: "",
    end_date: "",
    location: "",
    description: "",
    hrs: 0,
  });

  const [tags, setTags] = useState([]);

  function HandleEventChange(event) {
    setEventData({
      ...eventData,
      [event.target.name]: event.target.value,
      tags: tags,
    });
  }

  async function HandleSubmit(event) {
    event.preventDefault();
    try {
      const data = { ...eventData, tags: [...tags] };
      console.log("TOKEN DATA", token);
      console.log("EVENTDATA w TAGS", data);
      await EventsAPI.createEvent(data, token);
      setEventData({
        name: "",
        start_date: "",
        end_date: "",
        location: "",
        description: "",
        hrs: 0,
      });
      setTags([]);
    } catch (err) {
      console.log("ERROR SUBMITTING EVENT", err);
    }
  }

  function AddTag(event) {
    event.preventDefault();
    const newTag = event.target.elements.tagInput.value;
    setTags([...tags, newTag]);
    event.target.elements.tagInput.value = "";
  }

  function RemoveTag(index) {
    const newTags = [...tags];
    newTags.splice(index, 1);
    setTags(newTags);
  }

  return {
    eventData,
    tags,
    HandleSubmit,
    HandleEventChange,
    AddTag,
    RemoveTag,
  };
}

/* ======================================================== */
/* Main Component Functions: */

function EventCreateLogic() {
  const {
    eventData,
    tags,
    HandleEventChange,
    AddTag,
    RemoveTag,
    HandleSubmit,
  } = EventCreateHandler();

  const isAuthenticated = useAuth();

  if (!isAuthenticated) {
    return <div>You are not authorized to access this page.</div>;
  }
  return (
    <>
      <EventCreateHtml
        eventData={eventData}
        HandleEventChange={HandleEventChange}
        HandleSubmit={HandleSubmit}
        tags={tags}
        AddTag={AddTag}
        RemoveTag={RemoveTag}
      />
    </>
  );
}

/** Export the component function */
export default EventCreateLogic;
/* ======================================================== */
