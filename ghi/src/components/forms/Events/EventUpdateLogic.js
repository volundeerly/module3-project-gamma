import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useAuthContext } from "utility/Essentials/Authentication";
import { DebugLogger } from "utility/Misc/Helpers";
import { EventsAPI } from "api/EventsAPI";
import { ToggleScrollLock } from "utility/Misc/Helpers";

// Import the AccountsAPI to use API backend custom function calls
import AccountsAPI from "api/AccountsAPI";

function HTML(props) {
  let {
    eventData,
    HandleSubmit,
    HandleEventChange,
    addTag,
    removeTag,
    tags,
    volunteers,
    handleDelete,
    volunteerNames,
    token,
  } = props;

  return (
    <>
      <div className="grid-profile grid grid-cols-2 gap-10">
        <div className="max-w-md mx-auto volunteers-wrapper">
          <table className="theme-background-1 table-auto border-separate border-spacing-2 rounded-xl shadow-xl shadow-beige shadow-slate-300 shadow w-90 border border-slate-300 mt-4">
            <thead>
              <tr>
                <th className="h-8 w-full text-sm pl-2 bg-tansparent outline-blue text-center">
                  Name of Volunteers
                </th>
                <th className="h-8 w-full text-sm pl-2 bg-tansparent outline-blue text-center">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {volunteerNames?.map((volunteer, index) => (
                // [0 Isaac] [1 John]
                <tr key={index}>
                  <td className="font-medium uppercase text-brown h-8 w-full rounded-md border border-brown text-sm pl-2 bg-tansparent outline-blue text-center">
                    {volunteer}
                  </td>
                  <td className="inline-block rounded bg-brown px-7 pt-3 pb-2.5 text-sm font-medium uppercase leading-normal text-beige-light shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700">
                    <button onClick={() => handleDelete(volunteers[index], token)}>
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="eventcreate-wrapper ">
          <div className="items-center h-screen bg-slate-200">
            <div
              id="form"
              className="block theme-background-1 p-6 rounded-xl shadow-xl shadow-beige shadow-slate-300 shadow w-90"
            >
              <form onSubmit={HandleSubmit}>
                <h1 className="text-brown text-lg m-3 p-4 w-full text-center font-extrabold">
                  Update Event
                </h1>
                <label
                  htmlFor="name"
                  className="block text-brown text-sm font-bold mb-2"
                >
                  Event Name
                  <input
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                    value={eventData.name}
                    name="name"
                    type="text"
                    onChange={HandleEventChange}
                  />
                </label>
                <label
                  htmlFor="start_date"
                  className="block text-brown text-sm font-bold mb-2"
                >
                  Start Date
                  <input
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                    value={eventData.start_date}
                    name="start_date"
                    type="datetime-local"
                    onChange={HandleEventChange}
                  />
                </label>
                <label
                  htmlFor="end_date"
                  className="block text-brown text-sm font-bold mb-2"
                >
                  End Date
                  <input
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                    value={eventData.end_date}
                    name="end_date"
                    type="datetime-local"
                    onChange={HandleEventChange}
                  />
                </label>
                <label
                  htmlFor="location"
                  className="block text-brown text-sm font-bold mb-2"
                >
                  Location
                  <input
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                    value={eventData.location}
                    name="location"
                    type="text"
                    onChange={HandleEventChange}
                  />
                </label>
                <label
                  htmlFor="description"
                  className="block text-brown text-sm font-bold mb-2"
                >
                  Description
                  <input
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                    value={eventData.description}
                    name="description"
                    type="text"
                    onChange={HandleEventChange}
                  />
                </label>
                <label
                  htmlFor="hrs"
                  className="block text-brown text-sm font-bold mb-2"
                >
                  Total Hours
                  <input
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                    value={eventData.hrs}
                    name="hrs"
                    type="number"
                    onChange={HandleEventChange}
                  />
                </label>

                <div className="text-brown text-lg m-3 p-2 w-full text-center font-extrabold">
                  <button
                    type="submit"
                    className="inline-block rounded bg-beige-light px-7 pt-3 pb-2.5 text-sm font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                  >
                    Update Event
                  </button>
                </div>
              </form>
              <div>
                <form onSubmit={addTag}>
                  <input
                    type="text"
                    name="tagInput"
                    className="h-8 w-full rounded-md border border-slate-300 text-sm pl-2 bg-tansparent outline-blue"
                  />
                  <button
                    type="submit"
                    className="inline-block m-3 p-4 rounded bg-beige-light px-4 pt-2 pb-2.5 text-xs font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                  >
                    Add Tag
                  </button>
                </form>
                <ul>
                  {tags.map((tag, index) => (
                    <li
                      className="block w-3/5 mr-1 rounded-xl m-2 p-2 rounded bg-beige-light px-1 pt-1 pb-1 text-xs font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                      key={index}
                      style={{
                        text: "#ecfccb",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <span>{tag}</span>
                        <button
                          onClick={() => removeTag(index)}
                          type="submit"
                          className="inline-block m-2 p-2  rounded-xs  px-1 pt-1 pb-1 text-xs font-medium uppercase leading-normal text-brown shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] btn-xs"
                          style={{
                            backgroundColor: "#ecfccb",
                            marginLeft: "10px",
                          }}
                        >
                          Remove
                        </button>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
//Volunteer list

function VolunteersHandler() {
  const [volunteerNames, setVolunteerNames] = useState([]);
  const [volunteers, setVolunteers] = useState([]);;
  const { event_id } = useParams();

  async function handleDelete(volunteer_id, token) {
    try {
      await EventsAPI.removeVolunteer(event_id, volunteer_id, token);
      const filteredVolunteers = volunteerNames.filter(
        (volunteer) => volunteer.id !== volunteer_id
      );

      setVolunteerNames(filteredVolunteers);
    } catch (err) {}
  }

  async function getVolunteer(token) {
    try {
      const data = await EventsAPI.getVolunteers(event_id, token);
      console.log("Volunteers", data);
      setVolunteers(data);
    } catch (err) {
      console.log("ERROR FETCHING VOLUNTEERS", err);
    }
  }

  async function fetchVolunteersDetails() {
    // Returns an array of volunteer names
    const volunteersDetails = await Promise.all(
      volunteers.map(async (volunteer) => {
        const volunteerDetail = await AccountsAPI.GetVolunteerDetail(volunteer);
        return volunteerDetail.first_name;
      })
    );
    console.log("Volunteers Names", volunteersDetails);
    setVolunteerNames(volunteersDetails);
  }
  // [1,2]
  // [Isaac, John]

  return {
    fetchVolunteersDetails,
    handleDelete,
    volunteerNames,
    getVolunteer,
    volunteers,
  };
}

//

function UpdateEventHandler() {
  const { token } = useAuthContext();
  const { event_id } = useParams();
  const [eventData, setEventData] = useState({
    name: "",
    start_date: "",
    end_date: "",
    location: "",
    description: "",
    hrs: 0,
    id: event_id,
  });
  const [tags, setTags] = useState([]);

  function HandleEventChange(event) {
    setEventData({
      ...eventData,
      [event.target.name]: event.target.value,
      tags: tags,
    });
  }

  async function HandleSubmit(event) {
    event.preventDefault();
    try {
      const data = { ...eventData, tags: [...tags] };

      DebugLogger("TOKEN DATA:", token, "\n EVENTDATA W/ TAGS:", data);

      await EventsAPI.updateEvent(event_id, data, token);
      setEventData({
        name: "",
        start_date: "",
        end_date: "",
        location: "",
        description: "",
        hrs: 0,
      });
      setTags([]);
    } catch (err) {
      DebugLogger("ERROR SUBMITTING EVENT", err);
    }
  }

  function addTag(event) {
    event.preventDefault();
    const newTag = event.target.elements.tagInput.value;
    setTags([...tags, newTag]);
    event.target.elements.tagInput.value = "";
  }

  function removeTag(index) {
    const newTags = [...tags];
    newTags.splice(index, 1);
    setTags(newTags);
  }

  function HandleTagChange(newTags) {
    setTags(newTags);
  }

  return {
    eventData,
    tags,
    HandleSubmit,
    HandleEventChange,
    HandleTagChange,
    addTag,
    removeTag,
  };
}

function EventUpdateLogic() {
  const {
    eventData,
    tags,
    HandleEventChange,
    HandleTagChange,
    addTag,
    removeTag,
    HandleSubmit,
  } = UpdateEventHandler();

  DebugLogger("TAGS:", tags);
  //Volunteer stuff
  //volunteer stuff
  const { event_id } = useParams();
  const { token } = useAuthContext();
  const {
    handleDelete,
    fetchVolunteersDetails,
    volunteerNames,
    getVolunteer,
    volunteers,
  } = VolunteersHandler();

  useEffect(() => {
    getVolunteer(token);
  }, [token]);

  useEffect(() => {
    if (volunteers.length > 0) {
      fetchVolunteersDetails();
    }
  }, [volunteers]);

  ToggleScrollLock("scroll");
  return (
    <>
      <HTML
        eventData={eventData}
        HandleTagChange={HandleTagChange}
        HandleEventChange={HandleEventChange}
        HandleSubmit={HandleSubmit}
        tags={tags}
        addTag={addTag}
        removeTag={removeTag}
        volunteers={volunteers}
        handleDelete={handleDelete}
        volunteerNames={volunteerNames}
        token={token}
      />
    </>
  );
}
export default EventUpdateLogic;
