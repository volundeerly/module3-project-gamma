

function UpdateProfileHTML(props) {
    let { profile, HandleSubmit, HandleProfileChange } = props;
    return (
    <div className="signup-wrapper ">
      <div className="flex justify-center items-center h-screen bg-slate-200">
        <div
          id="form"
          className="block bg-slate-50 p-6 rounded-xl shadow-xl shadow-beige shadow-slate-300 w-90">
          <form onSubmit={HandleSubmit}>
          <div className="flex justify-center items-center h-screen bg-slate-200">
            <div className="py-8 flex-grow max-h-500">
              <h2 className="text-3xl font-bold mb-4 upcoming-events-heading">
                Update Profile:
              </h2>
              <h3 className="text-2xl font-bold mb-4 upcoming-events-heading">
                {`${profile.first_name} ${profile.last_name}`}
              </h3>
              <div className="flex flex-col h-full justify-center">
                <div className="flex-grow max-h-500">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                      <label>
                      <p>First Name</p>
                      <input
                        value={profile.first_name}
                        name="first_name"
                        type="text"
                        onChange={HandleProfileChange}
                      />
                      </label>
                      <label>
                        <p>Last Name</p>
                        <input
                          value={profile.last_name}
                          name="last_name"
                          type="text"
                          onChange={HandleProfileChange}
                        />
                      </label>
                      <label>
                        <p>Phone Number</p>
                        <input
                          value={profile.phone_num}
                          name="phone_num"
                          type="int"
                          onChange={HandleProfileChange}
                        />
                      </label>
                      <label>
                        <p>Zip Code</p>
                        <input
                          value={profile.zip_code}
                          name="zip_code"
                          type="int"
                          onChange={HandleProfileChange}
                        />
                      </label>
                      <label>
                        <p>Profile Pic</p>
                        <input
                          value={profile.profile_pic}
                          name="profile_pic"
                          type="text"
                          onChange={HandleProfileChange}
                        />
                      </label>
                      <label>
                        <p>Bio</p>
                        <input
                          value={profile.bio}
                          name="bio"
                          type="text"
                          onChange={HandleProfileChange}
                        />
                      </label>
                    </div>
                    <button type="submit">Update</button>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </form>
    </div>
    </div>
    </div>

  );

}
export default UpdateProfileHTML;
