import React, { useState } from "react";
import { useAuthContext } from "utility/Essentials/Authentication";
import AccountsAPI from "api/AccountsAPI";
import UpdateProfileHTML from "components/forms/Profile/UpdateProfileHtml"

function UpdateProfileHandler() {
  const { token } = useAuthContext();
  const [profileData, setProfileData] = useState({
    first_name: "",
    last_name: "",
    phone_num: 0,
    zip_code: 0,
    profile_pic: "",
    bio: "",
  });

  function HandleProfileChange(event) {
    setProfileData({
      ...profileData,
      [event.target.name]: event.target.value,
    });
  }

  async function HandleSubmit(event) {
    event.preventDefault();
    try {
      const account = { ...profileData };
      await AccountsAPI.updateAccount(account, token);
      setProfileData({
        first_name: "",
        last_name: "",
        phone_num: 0,
        zip_code: 0,
        profile_pic: "",
        bio: "",
      });
    } catch (err) {
      console.log("ERROR SUBMITTING ACCOUNT CHANGE", err);
    }
  }
  return {
    profileData,
    HandleProfileChange,
    HandleSubmit,
  }
}


function UpdateProfileLogic() {
  const {
    profileData,
    HandleProfileChange,
    HandleSubmit,
  } = UpdateProfileHandler();
  return (
    <>
      <UpdateProfileHTML
        profile={profileData}
        HandleProfileChange={HandleProfileChange}
        HandleSubmit={HandleSubmit}
      />
    </>
  );
}
export default UpdateProfileLogic;
