function FormInputTemplateHtml(props) {
  return (
    <label
      className={`${props.LabelClassName} volundeerly-font block text-brown text-sm font-bold m-3`}
      htmlFor={props.name}
    >
      {props.title}
      <input
        className={`${props.InputClassName} volundeerly-font bounce-hover shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
        value={props.value}
        name={props.name}
        onChange={props.onChange}
        id={props.name}
        type={props.type}
        pattern={props.pattern == null ? null : props.pattern}
        placeholder={props.placeholder == null ? null : props.placeholder}
        required={props.required}
        autoComplete="on"
        disabled={props.disabled}
      />
    </label>
  );
}

export default FormInputTemplateHtml;
