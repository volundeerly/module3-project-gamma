/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import React, { useState } from "react";
// Import the Modal Template Components
import {
  ModalButtonHtml,
  ModalHandlerLogic,
} from "utility/Importers/PageImports";
// Import the AccountModalHtml
import AccountModalHtml from "components/forms/Account/AccountModalHtml";
// Import the SignUpHandlerLogic for Functions
import SignupHandlerLogic from "components/forms/SignUp/SignUpHandlerLogic";
// Import the LoginHandlerLogic for Functions
import LoginHandlerLogic from "components/forms/Login/LoginHandlerLogic";

/* ======================================================== */
/* Micro Component Functions: */

function AccountFormHandler() {
  const [currentForm, setCurrentForm] = useState("");

  function FormSet(Form) {
    setCurrentForm(Form);
  }

  return { FormFunctions: { currentForm, FormSet } };
}

/* ======================================================== */
/* Main Component Functions: */

function AccountModalLogic() {
  /* ======================================================== */
  // Get an Object of Functions from the ModalHandlerLogic
  const ModalFunctions = ModalHandlerLogic();
  // Call the LoginHandler Micro Component and return the State altering Functions and Data.
  const { LoginFunctions } = LoginHandlerLogic(ModalFunctions);
  // Call the SignupHandler Micro Component and return the State altering Functions and Data.
  const { SignUpFunctions } = SignupHandlerLogic(ModalFunctions);
  // Call the AccountFormHandler Micro Component and return the State altering Functions and Data.
  const { FormFunctions } = AccountFormHandler();
  /* ======================================================== */

  // Return the HTML/JSX Data
  return (
    <React.Fragment>
      {/* Login Button */}
      <ModalButtonHtml
        type={"button"}
        text={"login"}
        classes={"button-size-1"}
        size="reg"
        onClick={() => {
          ModalFunctions.ShowModal();
          FormFunctions.FormSet("Login");
        }}
      />
      {/* Sign Up Button */}
      <ModalButtonHtml
        type={"button"}
        text={"sign up"}
        classes={"button-size-1"}
        size="reg"
        onClick={() => {
          ModalFunctions.ShowModal();
          FormFunctions.FormSet("SignUp");
        }}
      />
      {/* Modal Form */}
      {ModalFunctions.isShown ? (
        <AccountModalHtml
          /* ============================ */
          FormFunctions={FormFunctions}
          ModalFunctions={ModalFunctions}
          LoginFunctions={LoginFunctions}
          SignUpFunctions={SignUpFunctions}
          /* ============================ */
        />
      ) : null}
    </React.Fragment>
  );
  /* ======================================================== */
}

/** Export the component function */
export default AccountModalLogic;
/* ======================================================== */
