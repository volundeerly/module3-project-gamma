/* ======================================================== */
import FocusTrap from "focus-trap-react";
import React from "react";
import ReactDOM from "react-dom";

import SignUpModalHtml from "components/forms/SignUp/SignUpModalHtml";
import LoginModalHtml from "components/forms/Login/LoginModalHtml";

/* ======================================================== */
/* Main Component Function: */

/**
 * Reuseable Button Component:
 * - Settings Via Props:
 * @param {string} currentForm - the id or name to be given to the dom element
 * @param {string} linkTo - navigation link (route)
 * @param {string} classes - a string of classes to be added to the buttons
 * @param {string} size - size name to access specific css classes: "reg" or "small"
 * @param {string} type - button type: "button" or "submit"
 * @param {string} text - the text to be displayed in the button
 * @param {function} onClick - a function that would be fired when the button is clicked
 * @param {function} onHover - a function that would be fired when the button is hovered over
 */
function AccountModalHtml(props) {
  /*==========================================*/

  const { FormFunctions, LoginFunctions, SignUpFunctions, ModalFunctions } =
    props;

  const { OnKeyDown, OnClickOutside, isTransform, isOpacity, isScale } =
    ModalFunctions;

  /*==========================================*/

  return ReactDOM.createPortal(
    <FocusTrap>
      {/*===========================*/}
      <aside
        tag="aside"
        role="dialog"
        tabIndex="-1"
        aria-modal="true"
        id="modal-container"
        className="modal-wrapper modal-section center-please"
        onClick={OnClickOutside}
        onKeyDown={OnKeyDown}
      >
        {/*===========================*/}
        <div
          id="modal-container"
          tabIndex="-1"
          aria-hidden="true"
          className="modal-content-f fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full"
        >
          <div className="bounce-hover relative h-full max-w-2xl min-w-60 md:h-auto">
            {/*<!-- Modal content -->*/}
            <div
              className={`
              ${isTransform ? "" : "-translate-y-full"}
              ${isOpacity ? "" : "opacity-0"}
              ${isScale ? "" : "scale-150"}
                "transform transition-opacity transition-transform duration-300 theme-background-1 brown-border relative bg-white rounded-lg shadow-[0_35px_35px_-15px_rgba(0,0,0,0.3)] dark:bg-gray-700`}
            >
              {(() => {
                if (FormFunctions.currentForm === "Login") {
                  return (
                    <LoginModalHtml
                      LoginFunctions={LoginFunctions}
                      ModalFunctions={ModalFunctions}
                      FormFunctions={FormFunctions}
                    />
                  );
                } else if (FormFunctions.currentForm === "SignUp") {
                  return (
                    <SignUpModalHtml
                      SignUpFunctions={SignUpFunctions}
                      ModalFunctions={ModalFunctions}
                      FormFunctions={FormFunctions}
                    />
                  );
                }
              })()}
            </div>
          </div>
        </div>
      </aside>
      {/*===========================*/}
    </FocusTrap>,
    document.body
  );

  /*==========================================*/
}

/** Export the component function*/
export default AccountModalHtml;
/* ======================================================== */
