/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import { useState } from "react";
// Import the useToken authenticator for authenticating login.
import { useToken } from "utility/Essentials/Authentication";
// Helper function to check whether Button is Disabled
import { ButtonDisabled } from "utility/Misc/Helpers";
// Import useNavigate for Navigating to different pages
import { useNavigate } from "react-router-dom";

/* ======================================================== */
/* Main Component Functions: */

function LoginHandlerLogic(ModalFunctions) {
  /* ============================================= */
  // Set a loginData (formData) state
  const [loginData, setLoginData] = useState({ username: "", password: "" });
  // Get the login Function from the Authentication's useToken
  const { login } = useToken();
  // Assign useNavigate to a variable for ease of use.
  const navigate = useNavigate();
  /* ============================================= */

  // Function thats called when the Login Forms are typed in
  function HandleLoginChange(event) {
    setLoginData({
      ...loginData,
      [event.target.name]: event.target.value,
    });
  }

  // Function thats called when the Login Submit Button is pressed
  async function HandleLoginSubmit(event) {
    // Prevent default browser functionality.
    event.preventDefault();

    // Check if button is disabled
    if (ButtonDisabled(event, "check")) {
      return;
    }
    // Call the login function imported from the authenticator..
    try {
      // ====
      let [Status, Detail] = await login(
        loginData.username,
        loginData.password
      );
      // If Login was succesful
      if (Status) {
        setLoginData({ username: "", password: "" });
        // Close the Modal
        ModalFunctions.CloseModal();
        // Change Pages to the Home Page
        navigate("/profile/me");
      }
      // ==
      else {
        if (Detail === "No Data Retrieved") {
        } else if (Detail === "Incorrect username or password") {
        }
      }
    } catch (error) {
      console.error(error);
    }
  }

  function HandleOnClose() {
    setLoginData({ username: "", password: "" });
    ModalFunctions.CloseModal();
  }

  /* ============================================= */
  // Return the Functions
  return {
    LoginFunctions: {
      loginData,
      HandleOnClose,
      HandleLoginSubmit,
      HandleLoginChange,
    },
  };
}

/** Export the component function */
export default LoginHandlerLogic;
/* ======================================================== */
