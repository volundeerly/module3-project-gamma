/* ======================================================== */
/* Imports: */

// Import the FormInputTemplateHtml Component for InputBoxes
import FormInputTemplateHtml from "components/forms/Misc/FormInputTemplateHtml";
// Import the Button Htm Component For all Buttons
import { ModalButtonHtml } from "utility/Importers/PageImports";

/* ======================================================== */
/* Micro Component Functions: */

/** Modal Header HTML */
function ModalHeaderHtml(props) {
  return (
    <div className="w-50 items-start justify-between p-4 border-bottom-black rounded-t dark:border-black-600">
      <h3 className="volundeerly-font relative text-xl font-semibold center-please text-brown dark:text-white">
        LOGIN
      </h3>
      <h5 className="volundeerly-font relative text-sm center-please text-green dark:text-white">
        Login to your account.
      </h5>
    </div>
  );
}

/** Modal Body HTML */
function ModalBodyHtml(props) {
  // Get the Needed Functions and Props.
  const { LoginFunctions, FormFunctions } = props;

  return (
    <div className="relative m-2" data-te-input-wrapper-init>
      <form id="LoginForm" onSubmit={LoginFunctions.HandleLoginSubmit}>
        <FormInputTemplateHtml
          value={LoginFunctions.loginData.username}
          name={"username"}
          onChange={LoginFunctions.HandleLoginChange}
          title={"Email"}
          type="text"
          required={true}
          InputClassName="w-full"
        />
        <FormInputTemplateHtml
          value={LoginFunctions.loginData.password}
          name={"password"}
          onChange={LoginFunctions.HandleLoginChange}
          title={"Password"}
          type="password"
          required={true}
          InputClassName="w-full"
        />
        <button
          type="button"
          onClick={() => FormFunctions.FormSet("SignUp")}
          className="volundeerly-font bounce-hover block pb-5 pl-4 text-brown text-xs font-bold"
        >
          Dont have an account? Sign up!
        </button>
      </form>
    </div>
  );
}

/** Modal Footer HTML */
function ModalFooterHtml(props) {
  const { ButtonClick } = props;

  return (
    <div className="mb-2">
      <div className="relative mb-3 text-center">
        <ModalButtonHtml
          type={"submit"}
          name={"logout-button"}
          text={"Login"}
          classes={"button-size-1"}
          size={"reg"}
          form={"LoginForm"}
        />
        <ModalButtonHtml
          type={"button"}
          name={"close-button"}
          text={"Close"}
          classes={"button-size-1"}
          onClick={ButtonClick}
          size={"reg"}
        />
      </div>
    </div>
  );
}

/* ======================================================== */
/* Main Component Function: */

/** Modal HTML for the Login */
function LoginModalHtml(props) {
  const { LoginFunctions, FormFunctions, ModalFunctions } = props;

  return (
    <>
      {/*<!-- Modal header -->*/}
      <ModalHeaderHtml />
      {/*<!-- Modal body -->*/}
      <ModalBodyHtml
        LoginFunctions={LoginFunctions}
        FormFunctions={FormFunctions}
      />
      {/*<!-- Modal footer -->*/}
      <ModalFooterHtml ButtonClick={ModalFunctions.ButtonClick} />
    </>
  );
}

/** Export the component function */
export default LoginModalHtml;
/* ======================================================== */
