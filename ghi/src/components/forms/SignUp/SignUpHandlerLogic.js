/* ======================================================== */
/* Imports: */

// Import base React functions and classes
import { useState } from "react";
// Import the useToken authenticator for authenticating login.
import { useToken } from "utility/Essentials/Authentication";
// Helper function to check whether Button is Disabled
import { ButtonDisabled } from "utility/Misc/Helpers";
// Import useNavigate for Navigating to different pages
import { useNavigate } from "react-router-dom";

/* ======================================================== */
/* Main Component Functions: */

function SignupHandlerLogic(ModalFunctions) {
  /* ============================================= */
  // Set a signUpData (formData) state
  const [signUpData, setSignUpData] = useState({
    username: "",
    password: "",
    first_name: "",
    last_name: "",
    email: "",
    phone_num: "",
    zip_code: "",
  });
  // Get the signup Function from the Authentication's useToken
  const { signup } = useToken();
  // Assign useNavigate to a variable for ease of use.
  const navigate = useNavigate();
  /* ============================================= */

  // Function thats called when the SignUp Forms are typed in
  const HandleSignUpChange = (event) => {
    setSignUpData({
      ...signUpData,
      [event.target.name]: event.target.value,
    });
  };

  // Function thats called when the SignUp Submit Button is pressed
  const HandleSignUpSubmit = async (event) => {
    // Prevent default browser functionality.
    event.preventDefault();

    // Check if button is disabled
    if (ButtonDisabled(event, "check")) {
      return;
    }

    // Call the login function imported from the authenticator..
    try {
      // ====
      let [Status, Detail] = await signup(signUpData);
      // If Login was succesful
      if (Status) {
        setSignUpData({
          username: "",
          password: "",
          first_name: "",
          last_name: "",
          email: "",
          phone_num: "",
          zip_code: "0",
        }); // Close the Modal
        ModalFunctions.CloseModal();
        // Change Pages to the Home Page
        navigate("/profile/me");
      }
      // ==
      else {
        console.log(Detail);
        if (Detail === "No Data Retrieved") {
        } else if (Detail === "Incorrect username or password") {
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  function HandleOnClose() {
    setSignUpData({
      username: "",
      password: "",
      first_name: "",
      last_name: "",
      email: "",
      phone_num: "",
      zip_code: "0",
    });
    ModalFunctions.CloseModal();
  }

  /* ============================================= */
  // Return the Functions
  return {
    SignUpFunctions: {
      signUpData,
      HandleOnClose,
      HandleSignUpChange,
      HandleSignUpSubmit,
    },
  };
}

/** Export the component function */
export default SignupHandlerLogic;
/* ======================================================== */
