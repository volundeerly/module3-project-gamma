/* ======================================================== */
/* Imports: */

// Import the FormInputTemplateHtml Component for InputBoxes
import FormInputTemplateHtml from "components/forms/Misc/FormInputTemplateHtml";
// Import the Button Htm Component For all Buttons
import { ModalButtonHtml } from "utility/Importers/PageImports";

/* ======================================================== */
/* Micro Component Functions: */

/** Modal Header HTML */
function ModalHeaderHtml(props) {
  return (
    <div className="w-50 items-start justify-between p-4 border-bottom-black rounded-t dark:border-black-600">
      <h3 className="volundeerly-font relative text-xl font-semibold center-please text-brown dark:text-white">
        SIGN-UP
      </h3>
      <h5 className="volundeerly-font relative text-sm center-please text-green dark:text-white">
        Sign-Up and create an account.
      </h5>
    </div>
  );
}

/** Modal Body HTML */
function ModalBodyHtml(props) {
  // Get the Needed Functions and Props.
  const { SignUpFunctions, FormFunctions } = props;

  return (
    <div id="form" className="relative m-2">
      <form id="SignUpForm" onSubmit={SignUpFunctions.HandleSignUpSubmit}>
        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.first_name}
          name={"first_name"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"First Name"}
          type="text"
          required={true}
          InputClassName="w-full"
        />
        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.last_name}
          name={"last_name"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"Last Name"}
          type="text"
          required={true}
          InputClassName="w-full"
        />

        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.username}
          name={"username"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"Username"}
          type="text"
          required={true}
          LabelClassName={""}
          InputClassName="w-full"
        />

        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.password}
          name={"password"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"Password"}
          type="password"
          required={true}
          LabelClassName={""}
          InputClassName="w-full"
        />

        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.email}
          name={"email"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"Email"}
          type="email"
          placeholder="@"
          required={true}
          LabelClassName={""}
          InputClassName="w-full"
        />

        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.phone_num}
          name={"phone_num"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"Phone Number"}
          type="tel"
          pattern="[0-9]{10}"
          placeholder="###-###-####"
          required={true}
          maxLength={10}
          LabelClassName={""}
          InputClassName="w-full"
        />

        <FormInputTemplateHtml
          value={SignUpFunctions.signUpData.zip_code}
          name={"zip_code"}
          onChange={SignUpFunctions.HandleSignUpChange}
          title={"Zip Code"}
          type="number"
          autoComplete="postal-code"
          required={true}
          LabelClassName={""}
          InputClassName="w-full"
        />

        <button
          type="button"
          onClick={() => FormFunctions.FormSet("Login")}
          className="volundeerly-font bounce-hover block pb-5 pl-4 text-brown text-xs font-bold"
        >
          Have an account? Log in!
        </button>
      </form>
    </div>
  );
}

/** Modal Footer HTML */
function ModalFooterHtml(props) {
  const { currentForm, ButtonClick } = props;
  return (
    <div className="mb-2">
      <div className="relative mb-3 text-center">
        <ModalButtonHtml
          type={"submit"}
          name={"signup-button"}
          text={"Submit"}
          classes={"button-size-1"}
          size={"reg"}
          form={"SignUpForm"}
        />
        <ModalButtonHtml
          type={"button"}
          name={"close-button"}
          text={"Close"}
          classes={"button-size-1"}
          onClick={ButtonClick}
          size={"reg"}
        />
      </div>
    </div>
  );
}

/* ======================================================== */
/* Main Component Function: */

/** Modal HTML for the SignUp */
function SignUpModalHtml(props) {
  const { SignUpFunctions, FormFunctions, ModalFunctions } = props;

  return (
    <>
      {/*<!-- Modal header -->*/}
      <ModalHeaderHtml />
      {/*<!-- Modal body -->*/}
      <ModalBodyHtml
        SignUpFunctions={SignUpFunctions}
        FormFunctions={FormFunctions}
      />
      {/*<!-- Modal footer -->*/}
      <ModalFooterHtml ButtonClick={ModalFunctions.ButtonClick} />
    </>
  );
}

/** Export the component function */
export default SignUpModalHtml;
/* ======================================================== */
