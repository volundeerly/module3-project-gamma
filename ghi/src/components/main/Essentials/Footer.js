

import React from "react";
import "styles/index.css";

const Footer = () => {
  return (
    <div className="footer">
      <h1 className="bg-beige-light relative h-20 w-full text-small text-green text-center footer">
        Volundeerly: A Service Application Made By Future HackReactor Grads.
      </h1>
    </div>
  );
};
export default Footer;
