/* ======================================================== */
/* Imports: */

import React, { useState } from "react";
import { Transition } from "@headlessui/react";
import DeerlyLogo from "images/DeerlyLogoSVG.svg";
import { ModalButtonHtml } from "utility/Importers/PageImports";
import AccountModalLogic from "components/forms/Account/AccountModalLogic";
import LogoutModalLogic from "components/forms/Logout/LogoutModalLogic";
import { useAuthContext } from "utility/Essentials/Authentication";

/* ======================================================== */

function NavigationConditionalHtml(props) {
  return props.condition ? props.true : props.false;
}

/** Navbar Button Link Micro Component */
function NavigationElementHtml(props) {
  return (
    <td>
      <ModalButtonHtml
        linkTo={props.to}
        type={"button"}
        text={props.text}
        classes={"button-size-1"}
        size="reg"
      />
    </td>
  );
}

function NavigationTableHtml(props) {
  return (
    <table>
      <tbody>
        <tr>
          <NavigationElementHtml to={"/"} text={"Home"} />
          <NavigationConditionalHtml
            true={
              <td>
                <LogoutModalLogic />
              </td>
            }
            false={
              <td>
                <AccountModalLogic />
              </td>
            }
            condition={props.token}
          />
          <NavigationConditionalHtml
            true={
              <NavigationElementHtml to={"/profile/me"} text={"My Profile"} />
            }
            false={null}
            condition={props.token}
          />
          <NavigationElementHtml to={"/events/all"} text={"Area Events"} />
        </tr>
      </tbody>
    </table>
  );
}

function NavigationBarHtml(props) {
  const { token } = useAuthContext();

  return (
    <div>
      <div className="flex items-center">
        <h1 className="volundeerly-font text-2xl font-bold text-brown">
          Volundeerly
        </h1>

        <div className="hidden md:block">
          <div className="ml-10 flex items-baseline space-x-4">
            <NavigationTableHtml token={token} />
          </div>
        </div>
      </div>

      <div className="-mr-2 flex justify-center md:hidden">
        <button
          onClick={props.ToggleButton}
          type="button"
          className="bg-beige-light inline-flex items-center justify-center p-2 rounded-md text-brown hover:text-white hover:bg-beige focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-brown"
          aria-controls="mobile-menu"
          aria-expanded="false"
        >
          <span className="sr-only">Open Menu</span>
          {!props.isOpen ? (
            <svg
              className="block h-6 w-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              aria-hidden="true"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          ) : (
            <svg
              className="block h-6 w-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              aria-hidden="true"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          )}
        </button>
      </div>
    </div>
  );
}

function HandleNavbar() {
  const [isOpen, setIsOpen] = useState(false);
  const { token } = useAuthContext();

  function HandleNavButton(ref) {
    return (
      <div className="md:hidden" id="mobile-menu">
        <div ref={ref} className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
          <NavigationTableHtml token={token} />
        </div>
      </div>
    );
  }

  function ToggleButton() {
    setIsOpen(!isOpen);
  }
  return { isOpen, HandleNavButton, ToggleButton };
}

/* ======================================================== */

function Nav() {
  // Get HandleNavbar Functions
  const { isOpen, HandleNavButton, ToggleButton } = HandleNavbar();
  const { token } = useAuthContext();

  // Return the Nav JSX
  return (
    <nav className="Nav bg-beige-light p-2 navbar">
      {/* Navigation Bar */}
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex items-center justify-center h-16">
          <div className=" flex-shrink-0">
            <img className="h-16 w-16" src={DeerlyLogo} alt="Logo" />
          </div>
          <NavigationBarHtml
            token={token}
            isOpen={isOpen}
            ToggleButton={ToggleButton}
          />
        </div>
      </div>
      {/* Transition to Mobile UI */}
      <Transition
        show={isOpen}
        enter="transition ease-out duration-100 transform"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-75 transform"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        {HandleNavButton}
      </Transition>
      {/* ======================= */}
    </nav>
  );
}

/* ======================================================== */

export default Nav;
