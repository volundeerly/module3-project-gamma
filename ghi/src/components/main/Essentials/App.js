/* ======================================================== */
/* Imports: */

// Import the neccessary Route Components from the React-Router-Dom Module
import { BrowserRouter, Routes, Route } from "react-router-dom";
// Import all the Forms under the name "Forms" from the FormImports file to avoid individual imports.
import * as Forms from "utility/Importers/FormImports";
// Import all the Pages under the name "Pages" from the PageImports file to avoid individual imports.
import * as Pages from "utility/Importers/PageImports";
// Import all the RouteImports under the name "RouteImports" from the RouteImports file to avoid individual imports.
import * as RouteImports from "utility/Importers/RouteImports";
// Import the AuthProvider & useToken authenticator for authenticating login.
import { AuthProvider, useToken } from "utility/Essentials/Authentication";
// Import the Navbar.
import Navbar from "components/main/Essentials/Navbar";
// Import the Navbar.
import Footer from "components/main/Essentials/Footer";
// Import the Scroll Locker from the Helpers Functions
import { ToggleScrollLock } from "utility/Misc/Helpers";

/* ======================================================== */
/* Main Component Functions: */

/** Authentication Token Getter Function */
function GetToken() {
  useToken();
  return null;
}

/** Main React App function which renders all the routes. */
function App() {
  // Lock the scrolling on all pages
  ToggleScrollLock("locked");
  // Return the rendered pages
  return (
    <AuthProvider>
      <GetToken />
      <BrowserRouter>
        <Navbar />
        <Routes>
          {/* Home Route */}
          <Route path="/" element={<Pages.Home />} />

          {/* Page Routes */}

          <Route path="/events/*" element={<RouteImports.EventRoutes />} />

          <Route path="/profile/*" element={<RouteImports.ProfileRoutes />} />

          {/* 404 Handling of Routes */}
          <Route path="*" element={<Pages.ErrorPage />} />
          <Route path="" element={<Pages.ErrorPage />} />
          <Route element={<Pages.ErrorPage />} />
        </Routes>
      </BrowserRouter>
      <Footer />
    </AuthProvider>
  );
}

/** Export the React Component */
export default App;
/* ======================================================== */
