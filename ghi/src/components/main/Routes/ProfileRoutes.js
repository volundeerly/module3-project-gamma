/* ======================================================== */
/* Imports: */

// Import the neccessary Route Components from the React-Router-Dom Module
import { Routes, Route } from "react-router-dom";
// Import all the Pages under the name "Pages" from the PageImports file to avoid individual imports.
import * as Pages from "utility/Importers/PageImports";
import * as Forms from "utility/Importers/FormImports";
import Authenticated from "components/pages/Misc/Authenticated";
/* ======================================================== */
/* Main Component Functions: */

/** Main React App function which renders all the routes. */
function ProfileRoutes() {
  return (
    <Routes>
      <Route
        path=":account_param"
        element={
          <Authenticated>
            <Pages.Profile />
          </Authenticated>
        }
      />
      <Route
        path="me/update"
        element={
          <Authenticated>
            <Forms.UpdateProfile />
          </Authenticated>
        }
      />

      <Route path="" element={<Pages.ErrorPage />} />
    </Routes>
  );
}

/** Export the React Component */
export default ProfileRoutes;
/* ======================================================== */
