/* ======================================================== */
/* Imports: */

// Import the neccessary Route Components from the React-Router-Dom Module
import { Routes, Route } from "react-router-dom";
// Import all the Forms under the name "Forms" from the FormImports file to avoid individual imports.
import * as Forms from "utility/Importers/FormImports";
// Import all the Pages under the name "Pages" from the PageImports file to avoid individual imports.
import * as Pages from "utility/Importers/PageImports";
import Authenticated from "components/pages/Misc/Authenticated";

/* ======================================================== */
/* Main Component Functions: */

/** Main React App function which renders all the routes. */
function EventRoutes() {
  return (
    <Routes>
      <Route
        path="my"
        element={
          <Authenticated>
            <Pages.MyEvents />
          </Authenticated>
        }
      />
      <Route
        path="create"
        element={
          <Authenticated>
            <Forms.CreateEvent />
          </Authenticated>
        }
      />
      <Route path="all" element={<Pages.AllEvents />} />
      <Route path="detail/:event_id" element={<Pages.Event />} />
      <Route path=":event_id" element={<Forms.UpdateEvent />} />
      <Route path=":event_id/volunteers/" element={<Pages.VolunteerList />} />
      <Route path="" element={<Pages.ErrorPage />} />
    </Routes>
  );
}

/** Export the React Component */
export default EventRoutes;
/* ======================================================== */
