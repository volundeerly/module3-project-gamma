/* ================================================================ */
/* Imports: */

// Imports the DebugMode Boolean
import { DebugMode } from "utility/Essentials/Settings";

/* ================================================================ */
/* Utilites Functions: */

// Determines if string is a number
export function isNumeric(num) {
  return !isNaN(num);
}

/** Function for turning on or off the scrolling of the webpage.
 * Function can be given a purpose to specifically change to one locked state,
 * or the Purpose paramater can be voided to just allow for a toggle depending on
 * what scroll state, the document has currently  */
export function ToggleScrollLock(Purpose) {
  // If a purpose is sent to do a specific one.
  if (Purpose) {
    // If purpose is to turn on scroll, set overflow accordingly
    if (Purpose === "scroll") {
      document.body.style.overflow = "";
    }
    // If purpose is to turn off scroll, set overflow accordingly
    else if (Purpose === "locked") {
      document.body.style.overflow = "hidden";
    }
  } else {
    // Assign the overflow attribute to a variable
    let Overflow = document.body.style.overflow;

    // If scrolling, turn off scrolling
    if (Overflow === "") {
      // No Scroll
      document.body.style.overflow = "hidden";
    } else {
      // Scroll
      document.body.style.overflow = "";
    }
  }
}

// Checks whether the current mouse key is the escape key.
export function IsEscapeKey(event) {
  if (event.keyCode === 27) {
    return true;
  }
  return false;
}

// Makes sure a number doesnt go below the min or above the max.
export function Clamp(number, min, max) {
  return Math.max(min, Math.min(number, max));
}

// Logs whats past into the logs paramater, or any additional parameters. Only logs if the DebugMode is set to True.
export function DebugLogger(logs, ...additional) {
  if (DebugMode) {
    console.log(logs, additional);
  }
}

// Disables and checks for a button's disabled status, based on a cooldown.
export function ButtonDisabled(event, purpose, cooldown = 240) {
  if (event.target.disabled) {
    return true;
  }

  if (purpose === "check") {
    return false;
  }

  event.target.disabled = true;

  setTimeout(() => {
    event.target.disabled = false;
  }, cooldown);

  return false;
}

/* ================================================================ */
