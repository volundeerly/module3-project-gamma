import { Howl } from "howler";

import BubbleSoundFile from "sounds/bubble.mp3";
import arcade_tap from "sounds/arcade_tap_1.mp3";
import button_switch_35 from "sounds/button_switch_35.mp3";

import { Clamp } from "utility/Misc/Helpers";

//Howler.volume(0.1);

let SFX = {};

function SetupOtherSounds() {
  SFX.BubbleSound = new Howl({
    src: [BubbleSoundFile],
  });

  SFX.BubbleSound.volume(0.1);

  SFX.ArcadeTap1 = new Howl({
    src: [arcade_tap],
  });
  SFX.ArcadeTap1.volume(0.32);

  SFX.ButtonSwitch = new Howl({
    src: [button_switch_35],
  });
}

function SetupButtonClicks() {
  SFX.ButtonClicks = { Sounds: {} };

  for (let i = 1; i < 7; i++) {
    let button_sound_name = require(`sounds/arcade_button_${i}.mp3`);
    SFX.ButtonClicks.Sounds[i] = new Howl({
      src: [button_sound_name],
    });
    SFX.ButtonClicks.Sounds[i].volume(0.3);
  }

  SFX.length = Object.keys(SFX.ButtonClicks.Sounds).length;

  SFX.ButtonClicks["PlayRandomSound"] = function () {
    let random_index = Clamp(
      Math.floor(Math.random() * SFX.length + 1),
      1,
      SFX.length
    );
    SFX.ButtonClicks.Sounds[random_index].play();
  };
}

function SetupSounds() {
  SetupOtherSounds();
  SetupButtonClicks();
}

SetupSounds();

export default SFX;
