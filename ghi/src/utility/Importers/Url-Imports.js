const HostURL = process.env.REACT_APP_ACCOUNTS_HOST;

export const TokenURL = `${HostURL}/token`;

export const AccountsURL = `${HostURL}/accounts`;
export const AccountsDetailURL = `${AccountsURL}/detail`;

export const EventsURL = `${HostURL}/events`;
