import axios from "axios";
import { createContext, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as UrlStorage from "utility/Importers/Url-Imports";

let internalToken = null;
let internalAccountId = null;

export function getToken() {
  return internalToken;
}
export function getAccountId() {
  return internalAccountId;
}
// ======================================================= //

export async function getTokenInternal() {
  try {
    const response = await axios({
      method: "get",
      url: UrlStorage.TokenURL,
      withCredentials: true, // default
    });

    if (response.data === null) {
      return { token: null, account_id: null };
    }

    if (response.statusText === "OK") {
      internalToken = response.data.access_token;
      internalAccountId = response.data.account.id;

      return { token: internalToken, account_id: internalAccountId };
    }
  } catch (e) {
    console.log("GET TOKEN INTERNAL ERROR:", e);
    return { token: null, account_id: null };
  }
  return false;
}

export function useAuth() {
  const navigate = useNavigate();
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    const authenticate = async () => {
      try {
        const response = await fetch(UrlStorage.TokenURL, {
          credentials: "include",
        });

        if (!response.ok) {
          throw new Error("Authentication failed");
        }
        setAuthenticated(true);
      } catch (error) {
        navigate("/");
      }
    };

    authenticate();
  }, [navigate]);

  return authenticated;
}

// function handleErrorMessage(error) {
//   if ("error" in error) {
//     error = error.error;
//     try {
//       error = JSON.parse(error);
//       if ("__all__" in error) {
//         error = error.__all__;
//       }
//     } catch {}
//   }
//   if (Array.isArray(error)) {
//     error = error.join("<br>");
//   } else if (typeof error === "object") {
//     error = Object.entries(error).reduce(
//       (acc, x) => `${acc}<br>${x[0]}: ${x[1]}`,
//       ""
//     );
//   }
//   return error;
// }

// Dont call
export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  // Get the Local Token stored in LocalStorage
  let local_token = window.localStorage.getItem("account_token");

  const [token, setToken] = useState(local_token);
  const [account_id, setAccountId] = useState(internalAccountId);

  async function fetchToken() {
    const { token, account_id } = await getTokenInternal();
    setAccountId(account_id);
    setToken(token);
    window.localStorage.setItem("account_token", token);
  }

  useEffect(() => {
    if (!token) {
      fetchToken();
    }
  }, [token]);

  useEffect(() => {
    fetchToken();
  }, []);

  return (
    <AuthContext.Provider value={{ token, account_id, setAccountId, setToken }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);

export function useToken() {
  const { token, setToken } = useAuthContext();
  const { account_id, setAccountId } = useAuthContext();

  async function fetchToken() {
    const { token, account_id } = await getTokenInternal();

    setAccountId(account_id);
    setToken(token);
    window.localStorage.setItem("account_token", token);
    return true;
  }

  useEffect(() => {
    if (!token) {
      fetchToken();
    }
  }, [account_id, token, setAccountId, setToken]);

  async function logout() {
    if (token) {
      await fetch(UrlStorage.TokenURL, {
        method: "delete",
        credentials: "include",
      });
      internalToken = null;
      internalAccountId = null;

      setToken(null);
      setAccountId(null);

      return true;
    }
    return false;
  }

  async function login(username, password) {
    try {
      const form = new FormData();

      form.append("username", username);
      form.append("password", password);

      const response = await axios({
        method: "POST",
        url: UrlStorage.TokenURL,
        withCredentials: true,
        data: form,
      });

      if (response.statusText === "OK") {
        await fetchToken();
        return [true, "Successful login"];
      } else {
        return [false, "No Data Retrieved"];
      }
    } catch (error) {
      return [false, "Incorrect username or password"];
    }

    // let error = await response.json();
    return false;
  }

  async function signup(fieldData) {
    /*
    const form = new FormData();

    for (let [key, field] of Object.entries(fieldData)) {
      form.append(key, field);
    }

    */

    try {
      const response = await axios({
        method: "POST",
        url: UrlStorage.AccountsURL,
        withCredentials: true,
        data: JSON.stringify(fieldData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.statusText === "OK") {
        // Login the newly created account
        let [Status, Detail] = await login(fieldData.email, fieldData.password);

        if (Status) {
          return [Status, "Successful login and Signup"];
        } else {
          return [Status, "Successful Sign Up, failed Login"];
        }
      } else {
        return [false, "No Data Retrieved"];
      }
    } catch (error) {
      return [false, "Failed Signup"];
    }
  }

  async function update(username, password, email, firstName, lastName) {
    const response = await fetch(UrlStorage.AccountsURL, {
      method: "patch",
      body: JSON.stringify({
        username,
        password,
        email,
        first_name: firstName,
        last_name: lastName,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      await login(username, password);
    }
    return false;
  }
  return { token, account_id, login, logout, signup, update };
}

export const useUser = (token) => {
  const [user, setUser] = useState();

  useEffect(() => {
    if (!token) {
      return;
    }

    async function getUser() {
      const response = await fetch(UrlStorage.TokenURL, {
        credentials: "include",
      });
      if (response.ok) {
        const newUser = await response.json();
        setUser(newUser);
      }
    }

    getUser();
  }, [token]);

  return user;
};
