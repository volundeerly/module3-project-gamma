import axios from "axios";

const baseUrl = process.env.REACT_APP_ACCOUNTS_HOST;

export const EventsAPI = {
  async createEvent(event, token) {
    console.log("TOKENNNNN", token);
    try {
      const response = await axios.post(`${baseUrl}/events`, event, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async getAllEvents() {
    try {
      const fetchData = {
        credentials: "include",
      };
      const response = await axios.get(`${baseUrl}/events`, fetchData);
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async updateEvent(event_id, event, token) {
    try {
      const response = await axios.put(`${baseUrl}/events/${event_id}`, event, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async getEvent(event_id) {
    try {
      const fetchData = {
        credentials: "include",
      };

      const response = await axios.get(
        `${baseUrl}/events/detail/${parseInt(event_id)}`,
        fetchData
      );
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async getAccountEvents(token) {
    try {
      const response = await axios.get(`${baseUrl}/events/joined/account`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async getCreatedEvents(token) {
    try {
      const response = await axios.get(`${baseUrl}/events/created/account`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async deleteEvent(event_id, token) {
    try {
      const response = await axios.delete(
        `${baseUrl}/events/${parseInt(event_id)}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            credentials: "include",
          },
        }
      );
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async removeVolunteer(event_id, token) {
    try {
      const response = await axios.delete(
        `${baseUrl}/events/${parseInt(event_id)}/volunteers`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async getVolunteers(event_id, token) {
    try {
      const response = await axios.get(
        `${baseUrl}/events/${parseInt(event_id)}/volunteers`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },

  async addVolunteer(event_id, token) {
    try {
      const response = await axios.post(
        `${baseUrl}/events/${parseInt(event_id)}/volunteers`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
            credentials: "include",
          },
        }
      );
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },
};
