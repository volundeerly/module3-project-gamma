/* ======================================================== */
/* Imports: */

import axios from "axios";
import * as UrlStorage from "utility/Importers/Url-Imports";

/* ======================================================== */

const AccountsAPI = {
  // ================================ //

  GetProfileData: async (account_id) => {
    try {
      const response = await axios.get(
        `${UrlStorage.AccountsDetailURL}/${account_id}`
      );

      if ((response.statusText = "OK")) {
        return response.data;
      } else {
        return "No Data Retrieved";
      }
    } catch (error) {
      throw error.response.data.detail;
    }
  },
  // ================================ //
  GetVolunteerDetail: async (accounts_id) => {
    try {
      const response = await axios.get(
        `${UrlStorage.AccountsURL}/detail/${accounts_id}`
      );
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },
  // ================================ //
  async updateAccount(account, token) {
    try {
      const response = await axios.put(`${UrlStorage.AccountsURL}/accounts`, account, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    } catch (error) {
      throw error.response.data.detail;
    }
  },
};

export default AccountsAPI;
