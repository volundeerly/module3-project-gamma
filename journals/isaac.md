Date: 09 Mar, 2023
The group has been actively working on the frontend development of the project. Several commits were made, including more CSS changes, working on joining as a volunteer, and adding the option to join as a volunteer through all events page. Additionally, we worked to solve the tags issue and work on signup and create event pages. The team also experimented with modals and added a new logout modal with logout functionality.

Date: 08 Mar, 2023
The frontend development was continued, fixing the volunteers' list, adding CSS coloring to the body, adding a filter and detail modal to the all events page, and making the footer stick to the bottom. The group also organized/created more folders, fixed the navigation bar to use link to tags, and tested a commit.

Date: 07 Mar, 2023
 Work was continued on the volunteer list, fixing merge conflicts and still working on it. Basic URL authentication based on token was added and a quick fix was made, as well as adding and removing volunteer API functionality. Additionally, work was done on the home page, login page, navigation, and footer.

Date: 06 Mar, 2023
Work was done on the login page, updating the CSS and also working on the navigation and login CSS. The remove volunteer API functionality was also added.

Date: 05 Mar, 2023
Added the "My events" page which was almost fully functional.

Date: 04 Mar, 2023
Work was done on the list of volunteers, getting halfway done. Tailwind CSS was added for the all events page and applied to the events all/personal. Profile endpoints were also added and the schema for accounts was reworked.

Date: 03 Mar, 2023
Merge errors were resolved, and the CreateEvent.js was separated into EventCreateLogic and EventCreateHtml. Authentication was updated and a profile page was added. Additionally, changes were merged, a show event detail feature was added, and work was started on the list all events page. The event form was also made functional with token.

Date: 28 Feb, 2023
Cleaned up and tested the Events/Accounts API, ensuring that it was functioning smoothly. Additionally, a rating/ratings column without functionality was added to the API. Login was fixed with a new FormData()

Date: 27 Feb, 2023
The account model was updated, ensuring that it was up-to-date with the latest changes. Additionally, the ability to delete events from an account was added.

Date: 25 Feb, 2023
Made an update to the volunteers list, ensuring that it now held the volunteers' IDs.

Date: 24 Feb, 2023
Some minor changes were made and a backup feature was implemented. Account deletion was also implemented.

Date: 23 Feb, 2023
Small changes were made to the codebase, ensuring that it was functioning. Additionally, they implemented the GET/POST/DELETE/PUT features for events, which were partially tested.

On 21 Feb, 2023:
Made a minor change to fix the event.post router and implemented events routes. Additionally, we implemented the GET/POST/DELETE features for accounts, performed a pipeline test, and cleaned up the Create events API. We added a list events API, showing the events of the logged-in user, and worked on the events backend to complete the API for creating events only when logged in.

On 17 Feb, 2023:
We implemented the Get token feature.

On 16 Feb, 2023:
We completed the initial authentication with token.

On 15 Feb, 2023:
We created the API for accounts, deleted old migration file, merged create tables with main, fixed table schemas, and set up the Postgres initial table migration file.

On 14 Feb, 2023:
Reordered the cmd code of the Dockerfile.dev in the Volundeerly directory and completed the PostgreSQL database setup. We also completed the Postgre database with pgadmin.

On 6-13 Feb, 2023:
We decided to build an app that will allow anyone to create an account, create events that registered users can volunteer for, allows to volunteer for someone's else event or events, it will also allow users to see the events that are happening around the area, allow the owner of the event to remove volunteers, allows owner of event to see a list of the volunteers for a particular event that they created. A wireframe was made that can be found in the readme.txt
