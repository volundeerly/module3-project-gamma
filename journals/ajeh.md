Date: 09 Mar, 2023
Today we worked on a few CSS changes, worked on joining an event as a volunteer, and adding the option to join as a volunteer through all events page. Additionally, we worked to solve the tags issue and work on signup and create event pages. The team also experimented with modals and added a new logout modal with logout functionality.

Date: 08 Mar, 2023
We continued to work on the frontend, fixing the volunteers' list, adding CSS coloring to the body, adding a filter and detail modal to the all events page, and making the footer stick to the bottom. The group also organized/created more folders, fixed the navigation bar to use link to tags, and tested a commit.

Date: 07 Mar, 2023
We continued to work on the volunteer list, fixing merge conflicts and still working on it. Basic URL authentication based on token was added and a quick fix was made, as well as adding and removing volunteer API functionality. Additionally, work was done on the home page, login page, navigation, and footer.

Date: 06 Mar, 2023
We worked on the login page, updating the CSS and also working on the navigation and login CSS. The remove volunteer API functionality was also added.

Date: 05 Mar, 2023
Today the "My events" page took a while but it is almost fully functional.

Date: 04 Mar, 2023
We continued to work on the volunteers list logic. We started adding tailwind css to all of our html that we has so far. We also coded profile endpoints and the schema for accounts was reworked.

Date: 03 Mar, 2023
Today the team resolved a bunch of merge errors, and the CreateEvent.js was separated into EventCreateLogic and EventCreateHtml, which helps me understand things a bit further. Authentication was updated and a profile page was added. Additionally, changes were merged, a show event detail feature was added, and work was started on the list all events page. The event form was also made functional with access to the token.

Date: 28 Feb, 2023
Today we cleaned up and tested the Events/Accounts API, tom make sure that it was functioning smoothly. We also added a rating/ratings column without functionality to the API. We fixed our login with a new FormData().

Date: 27 Feb, 2023
We updated the account model, ensuring that it was up-to-date with the latest changes. Additionally, the ability to delete events from an account was added.

Date: 25 Feb, 2023
We made an update to the volunteers list, ensuring that it now held the volunteers' IDs. We then coded the accounts logic.

Date: 24 Feb, 2023
We did some debugging and more changes to our code. I believe a backup feature was implemented, as well as, account deletion. We also made the volunteer logic.

Date: 23 Feb, 2023
I don't really know what I did wrong the last time but after starting the setup process completely over, I am able to see what everyone else see!! I took the day to pull the most recent code and re-migrate everything. As a team, we made some more small changes were made to the backend code. Additionally, they implemented the GET/POST/DELETE/PUT features for events, which were tested more.

On 21 Feb, 2023:
As a team (on someone else's cooperative computer of course) we created the logic for posting, listing, and deleting events and the appropriate routes for them. We also made some necessary adjustments to the EventsAPI. We also added a feature for a specific user logged in to see "their" events which are the ones they signed up for. Then we created the restriction on creating events so that you can only do so logged in.

On 17 Feb, 2023:
Now that we have the initial auth token, we worked on implemented the Get token feature. I think we got it but my computer has not been working for the same code that works for everyone else so I have been doing some solo searching to see if there is some specific step needed for new Macs. I finally saw the extra command explained in Learn, but I am still having issues. I will start a fresh download of the code and try that command.

On 16 Feb, 2023:
Today wasn't an easy one...we took a while but were able to complete the initial authentication with token. I will say the more issues the better because I learn more and more how to debug.

On 15 Feb, 2023:
Today we did a few more backend steps. We created the API for accounts, deleted old migration file, merged create tables with main, fixed table schemas, and set up the Postgres initial table migration file. I have to say I love the group I am in because we work together well and help each other understand what we are doing as we go along.

On 14 Feb, 2023:
We followed the setup instructions in Learn to reordered the cmd code of the Dockerfile.dev in the Volundeerly directory. We also completed the postgreSQL database setup and connected it with our "pgadmin".

On 6-13 Feb, 2023:
My group and I decided on the app we were going to build. Its called Volundeerly (hence: the little baby deer logo) and it allows people to sign up and volunteer for any event posted or post and event to attract volunteers. We have the wireframe we created in readme.txt
