Date: 11 Mar, 2023
The development team made progress on the frontend development of a new project, including adding CSS changes to improve the appearance of the site. They also added the ability for users to create an account and join as a member through all events page. The team worked on implementing a filtering system and adding a detail modal to the all events page. They also experimented with modals and added a new logout modal with logout functionality.

Date: 10 Mar, 2023
The team continued to work on the frontend development, fixing bugs related to the volunteers' list and improving the navigation bar. They also implemented basic URL authentication based on token and added and removed volunteers' functionality. The team worked on creating a home page, login page, and footer.

Date: 09 Mar, 2023
Work was continued on the volunteers' list, fixing merge conflicts, and improving its functionality. The team also worked on the schema for accounts and added profile endpoints. They also made changes to the login page and navigation and footer.

Date: 08 Mar, 2023
The team worked on the list of volunteers and implemented Tailwind CSS for the all events page. They also added a "My events" page and made updates to the account model. The team ensured that the Events/Accounts API was functioning smoothly and added a rating/ratings column without functionality.

Date: 07 Mar, 2023
The team made progress on the CreateEvent API and separated CreateEvent.js into EventCreateLogic and EventCreateHtml. They also added a show event detail feature and started working on the list all events page. The team also ensured that the event form was functional with token authentication.

Date: 06 Mar, 2023
The team worked on fixing merge errors and made changes to the authentication system. They added a profile page and began implementing the list all events page. Additionally, they ensured that the ability to delete events from an account was functional.

Date: 05 Mar, 2023
The team continued to work on the volunteers' list, ensuring that it held the volunteers' IDs. They also implemented account deletion and added a backup feature.

Date: 04 Mar, 2023
The team made minor changes to the codebase and implemented the GET/POST/DELETE/PUT features for events. They also tested the Events/Accounts API and made sure it was functional.

Date: 03 Mar, 2023
The team implemented the GET/POST/DELETE features for accounts and added a list events API. They also worked on the events backend to ensure that events could only be created when logged in.

On 02 Mar, 2023:
The team discussed and planned a new project that would allow users to create an account, create events, and volunteer for events. They created a wireframe to outline the project's features and functionality.

Date: 28 Feb, 2023
Conducted testing on the application's APIs related to events and accounts, ensuring that they are functioning smoothly. Added a new rating/ratings feature to the events API, although it is not yet functional. Fixed the login feature by implementing a new FormData() method.

Date: 27 Feb, 2023
Updated the account model to incorporate the latest changes and added a new feature that allows users to delete events from their account.

Date: 25 Feb, 2023
Modified the volunteers list feature to include the volunteers' name.

Date: 24 Feb, 2023
Implemented a remove volunteer feature and a new account deletion feature.

Date: 23 Feb, 2023
Performed minor updates to the codebase and worked on testing the GET/POST/DELETE/PUT features for events, although they are only partially tested.

On 21 Feb, 2023:
Made minor adjustments to the event.post router and added events routes to the application. Implemented the GET/POST/DELETE features for accounts, conducted a pipeline test, and cleaned up the Create events API. Also added a new list events API, which displays the events of the logged-in user, and continued work on the events backend to restrict the API for creating events only when logged in.

On 17 Feb, 2023:
Implemented a new Get token feature for the application.

On 16 Feb, 2023:
Completed the initial authentication process using tokens for the application.

On 15 Feb, 2023:
Created the API for accounts, deleted old migration files, merged create tables with the main codebase, fixed table schemas, and set up the Postgres initial table migration file for the application.

On 14 Feb, 2023:
Reorganized the cmd code of the Dockerfile.dev in the Volundeerly directory and completed the PostgreSQL database setup using pgadmin.

On 6-13 Feb, 2023:
Conducted planning for the development of an application that will enable users to create accounts, create and volunteer for events, view events happening in their area, allow event owners to remove volunteers, and enable event owners to view a list of the volunteers for a specific event. Created a wireframe for the application, which can be found in the readme.txt file.