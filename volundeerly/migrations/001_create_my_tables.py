steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name  VARCHAR(100) NOT NULL,
            last_name VARCHAR(100) NOT NULL,
            username  VARCHAR(100) NOT NULL UNIQUE,
            password VARCHAR(100) NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE,
            phone_num TEXT NOT NULL,
            zip_code INTEGER NOT NULL CHECK (zip_code >= 00000 AND zip_code <= 99999),
            profile_pic TEXT,
            bio TEXT,
            rating_average INT[],
            created_events INT[] DEFAULT '{}'::integer[],
            joined_events INT[] DEFAULT '{}'::integer[]
        );

        CREATE TABLE events (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            start_date DATE NOT NULL,
            end_date DATE NOT NULL,
            location TEXT NOT NULL,
            description TEXT NOT NULL,
            tags TEXT[] NOT NULL,
            hrs INTEGER NOT NULL,
            volunteers TEXT[],
            account_id INTEGER NOT NULL REFERENCES accounts(id),
            rating NUMERIC(3, 2) DEFAULT 0.0,
            ratings NUMERIC[] CHECK (array_length(ratings, 1) = array_length(ratings::numeric[], 1))
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        DROP TABLE events;
        """
    ]
]
