from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountRepository,
    ProfileOut,
)

account_repo = AccountRepository()


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


# GET ROUTES
@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get("/accounts/detail/{account_id}", response_model=ProfileOut)
async def get_account_detail(
    account_id: str, repo: AccountRepository = Depends()
) -> ProfileOut:
    account = repo.get_by_id(account_id)
    if isinstance(account, dict):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=account["message"]
        )
    return account


# POST ROUTES
@router.post("/accounts", response_model=AccountToken)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepository = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password)
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.email, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


# PUT ROUTES
@router.put("/accounts", response_model=AccountOut)
async def update_account(
    account_form: AccountIn,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    repo: AccountRepository = Depends(),
) -> AccountOut:
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to update your account.",
        )
    try:
        updated_account = repo.update(current_account["email"], account_form)
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot update account with those credentials",
        )
    return updated_account


# DELETE ROUTES
@router.delete(
    "/accounts/{account_id}", status_code=status.HTTP_204_NO_CONTENT
)
async def delete_account(
    account_id: int,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    account_repo: AccountRepository = Depends(),
):
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to delete an account",
        )

    account = account_repo.get_by_id({account_id})

    if isinstance(account, dict):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=account["message"]
        )
    print("CURRENT_ACCOUNT ID", current_account["id"])
    if account.id != current_account["id"]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You don't have permission to delete this account",
        )
    account_repo.delete(account_id)
