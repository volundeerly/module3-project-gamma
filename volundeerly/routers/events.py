from fastapi import (
    Depends,
    APIRouter,
    HTTPException,
    status,
)

from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from typing import List, Optional
from queries.accounts import AccountOut, AccountRepository

from queries.events import (
    Event,
    EventOut,
    EventUpdate,
    EventCreate,
    EventRepository,
)


class AccountToken(Token):
    account: AccountOut


router = APIRouter()


# Protected created an event, must be logged in.
@router.post("/events", response_model=EventOut)
async def create_event(
    event: EventCreate,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    event_repo: EventRepository = Depends(),
    account_repo: AccountRepository = Depends(),
):
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create an event.",
        )

    try:
        created_event = event_repo.create(
            event=event, account_id=current_account["id"]
        )
        print("CREATED EVENT", created_event)
        account_repo.add_event_to_account(
            current_account["id"], created_event.id
        )
        created_event.volunteers.append(current_account["id"])
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create event with those credentials",
        )
    print("CREATED EVENT", created_event)
    return EventOut(id=created_event.id, **event.dict())


# Protect get all events associated to an account
# "/events/"
@router.get("/events", response_model=List[Event])
async def get_all_events(repo: EventRepository = Depends()) -> List[Event]:
    return repo.get_all()


# "/events/X"
# Unprotected get details of a specific event
@router.get("/events/detail/{event_id}", response_model=Event)
async def get_event_detail(event_id: int, repo: EventRepository = Depends()):
    event = repo.get_by_id(event_id)
    print("EVENTROUTE", event)
    return event


# get all events associated to an account
# "/events/X"
@router.get("/events/created/account", response_model=List[Event])
async def get_account_events(
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    repo: EventRepository = Depends(),
) -> List[Event]:
    return repo.get_by_account(current_account["id"])


@router.get("/events/joined/account", response_model=List[Event])
async def get_account_joined_events(
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    repo: EventRepository = Depends(),
) -> List[Event]:
    return repo.get_joined_events_by_account(current_account["id"])


# Protected update a specific event
@router.put("/events/{event_id}", response_model=EventUpdate)
async def update_event(
    event_id: int,
    event: EventUpdate,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    event_repo: EventRepository = Depends(),
):
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to update an event.",
        )
    try:
        updated_event = event_repo.update(event_id, event)
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot update event with those credentials.",
        )
    return updated_event


# Unprotected Get a list of volunteers in a specific event
@router.get("/events/{event_id}/volunteers", response_model=List[int])
async def get_volunteers_for_event(
    event_id: int, repo: EventRepository = Depends()
):
    event = repo.get_by_id(event_id)
    return event.volunteers


@router.post("/events/{event_id}/volunteers", response_model=EventOut)
async def sign_up_as_volunteer(
    event_id: int,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    event_repo: EventRepository = Depends(),
    account_repo: AccountRepository = Depends(),
):
    print("HELLO TEST")
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to join an event.",
        )
    try:
        event = event_repo.get_by_id(event_id)
        if current_account["id"] in event.volunteers:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="You have already signed \
                    up as a volunteer for this event",
            )
        print("EVENT", event)
        account_repo.add_to_joined_events(current_account["id"], event.id)
        print("EVENT", event)
    except Exception as e:
        print("ERROR", e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot Join this event",
        )
    event.volunteers.append(current_account["id"])

    update_event = event_repo.update(event_id, event)
    return update_event


@router.delete("/events/{event_id}/volunteers", response_model=EventOut)
async def remove_volunteer_self(
    event_id: int,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    event_repo: EventRepository = Depends(),
    account_repo: AccountRepository = Depends(),
):
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to remove a volunteer.",
        )

    event = event_repo.get_by_id(event_id)

    if current_account["id"] not in event.volunteers:
        if isinstance(event, dict):
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail=event["message"]
            )

    if current_account["id"] == event.account_id:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Creator of volunteer cannot remove \
                  themselves as volunteer",
        )
    else:
        account_repo.remove_from_joined_events(current_account["id"], event_id)
        event.volunteers.remove(current_account["id"])

    updated_event = event_repo.update(event_id, event)

    return updated_event


@router.delete(
    "/events/{event_id}/volunteers/{volunteer_id}", response_model=EventOut
)
async def remove_volunteer_other(
    event_id: int,
    volunteer_id: Optional[int] = None,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    event_repo: EventRepository = Depends(),
    account_repo: AccountRepository = Depends(),
):
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to remove a volunteer.",
        )
    try:

        event = event_repo.get_by_id(event_id)
        if current_account["id"] != event.account_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="You are not authorized to remove a \
                    volunteer from this event",
            )

        if volunteer_id not in event.volunteers:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="The volunteer is not registered for this event.",
            )

        account_repo.remove_from_joined_events(volunteer_id, event.id)
        event.volunteers.remove(volunteer_id)
        updated_event = event_repo.update(event_id, event)
        return updated_event

    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot remove volunteer from this event",
        )


# Protect Delete a specific event
@router.delete("/events/{event_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_event(
    event_id: int,
    current_account: AccountToken = Depends(
        authenticator.get_current_account_data
    ),
    event_repo: EventRepository = Depends(),
    account_repo: AccountRepository = Depends(),
):
    if not current_account:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to delete an event.",
        )

    event = event_repo.get_by_id(event_id)
    print("EVENT", type(event))
    if isinstance(event, dict):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=event["message"]
        )
    if event.account_id != current_account["id"]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You don't have permission to delete this event.",
        )
    account_repo.remove_from_created_events(current_account["id"], event_id)
    event_repo.delete(event_id)
