from pydantic import BaseModel
from queries.pool import pool
from typing import Dict, List, Optional


class Error(BaseModel):
    message: str


class Account(BaseModel):
    first_name: str
    last_name: str
    username: str
    password: str
    email: str
    phone_num: int
    zip_code: int
    profile_pic: str
    bio: str


class AccountIn(BaseModel):
    first_name: str
    last_name: str
    username: str
    password: str
    email: str
    phone_num: int
    zip_code: int


class AccountOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    phone_num: int
    zip_code: int


class ProfileOut(BaseModel):
    id: int
    username: str
    email: str
    first_name: str
    last_name: str
    phone_num: int
    zip_code: int
    profile_pic: Optional[str] = None
    bio: Optional[str] = None
    rating_average: Optional[int] = None
    created_events: Optional[List[str]] = None
    joined_events: Optional[List[str]] = None


class AccountWithPassword(AccountIn):
    id: int


class AccountRepository:
    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO accounts
                            (first_name, last_name, username,\
                                  password, email, phone_num, zip_code)
                        VALUES
                            (%s,%s,%s,%s,%s,%s,%s)
                        RETURNING id;
                        """,
                        [
                            info.first_name,
                            info.last_name,
                            info.username,
                            hashed_password,
                            info.email,
                            info.phone_num,
                            info.zip_code,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = info.dict()
                    return AccountOut(id=id, **old_data)

        except Exception:
            return {"message": "error!"}

    # Get account information (Used by authenticator)
    def get(self, email: str) -> AccountWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, first_name,last_name, username, email,\
                              phone_num, zip_code, profile_pic, bio, password\
                        FROM accounts
                        WHERE email = %s;
                        """,
                        [email],
                    )
                    record = db.fetchone()

                    if record:
                        columns = [desc[0] for desc in db.description]
                        data = dict(zip(columns, record))
                        return AccountWithPassword(**data)
                    else:
                        return {
                            "message": f"No account found with email {email}"
                        }
        except Exception as e:
            return {"message": "Could not get account"}

    def get_by_id(self, account_id: int) -> ProfileOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, first_name, last_name, phone_num, zip_code\
                            , profile_pic, bio, rating_average, \
                                created_events,\
                                joined_events, username, email
                        FROM accounts
                        WHERE id = %s;
                        """,
                        [account_id],
                    )
                    record = db.fetchone()
                    if record:
                        columns = [desc[0] for desc in db.description]
                        data = dict(zip(columns, record))
                        return ProfileOut(**data)
                    else:
                        print("ELSE PRINT")
                        return {
                            "message": f"No account found with id {account_id}"
                        }
        except Exception as e:
            print(e)
            return {"message": "Could not get account"}

    def get_by_name(self, name: str) -> List[ProfileOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, email, phone_num, zip_code,\
                            profile_pic, bio, reputation, events,\
                            username, email
                        FROM accounts
                        WHERE name ILIKE %s;
                        """,
                        [f"%{name}%"],
                    )
                    record = db.fetchone()
                    if record:
                        columns = [desc[0] for desc in db.description]
                        data = dict(zip(columns, record))
                        return ProfileOut(**data)
                    else:
                        return {
                            "message": f"No account found with name {name}"
                        }
        except Exception as e:
            print(e)
            return {"message": "Could not get account"}

    # Add event to both created and joined events list.
    # Returns: None
    def add_event_to_account(self, account_id: int, event_id: int) -> None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE accounts
                    SET created_events = array_append(created_events, %s)
                    WHERE id = %s;
                    """,
                    [event_id, account_id],
                )
                db.execute(
                    """
                    UPDATE accounts
                    SET joined_events = array_append(joined_events, %s)
                    WHERE id = %s;
                    """,
                    [event_id, account_id],
                )

    # Add event to accounts joined events list. Used to join as a volunteer
    # Returns: None
    def add_to_joined_events(self, account_id: int, event_id: int) -> None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE accounts
                    SET joined_events = array_append(joined_events, %s)
                    WHERE id = %s;
                    """,
                    [event_id, account_id],
                )

    # Remove event from accounts joined events list
    # Returns: None
    def remove_from_joined_events(
        self, account_id: int, event_id: int
    ) -> None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE accounts
                    SET joined_events = array_remove(joined_events, %s)
                    WHERE id = %s
                    """,
                    [event_id, account_id],
                )

    # Remove event from created events list on account
    # Returns: None
    def remove_from_created_events(
        self, account_id: int, event_id: int
    ) -> None:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE accounts
                    SET created_events = array_remove(created_events, %s)
                    WHERE id = %s;
                    """,
                    [event_id, account_id],
                )

    # Updates account information by email of account
    # Returns: AccountOut
    # id: int
    # first_name: str
    # last_name: str
    # email: str
    # phone_num: int
    # zip_code: int
    def update(self, email: str, info: AccountIn) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE accounts
                        SET
                            name = %s,
                            username = %s,
                            email = %s,
                            phone_num = %s,
                            zip_code = %s,
                            profile_pic = %s,
                            bio = %s
                        WHERE email = %s
                        RETURNING id;
                        """,
                        [
                            info.name,
                            info.username,
                            info.email,
                            info.phone_num,
                            info.zip_code,
                            info.profile_pic,
                            info.bio,
                            email,
                        ],
                    )
                    conn.commit()
                    id = result.fetchone()[0]
                    old_data = info.dict()
                    return AccountOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not update account."}

    # Deletes an account based on account id
    # Returns: true
    def delete(self, id: int) -> Dict[str, str]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            DELETE FROM accounts
                            WHERE id = %s;
                            """,
                        [id],
                    )
                    conn.commit()
                    return {
                        "message": f"Account with email {id} has been deleted."
                    }
        except Exception as e:
            print(e)
            return {"message": "Could not delete account."}
