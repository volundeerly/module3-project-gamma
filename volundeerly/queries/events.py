from pydantic import BaseModel
from queries.pool import pool
from datetime import datetime
from typing import List, Union, Dict
from collections import namedtuple


class Volunteer(BaseModel):
    id: int
    name: str


class Event(BaseModel):
    id: int
    name: str
    start_date: datetime
    end_date: datetime
    location: str
    description: str
    tags: List[str]
    hrs: int
    volunteers: Union[List[int], None] = []
    account_id: int
    rating: float = 0.0
    ratings: Union[List[float], None] = []
    # ratings: List[float] = [] | None


class EventCreate(BaseModel):
    name: str
    start_date: datetime
    end_date: datetime
    location: str
    description: str
    tags: List[str]
    hrs: int
    volunteers: List[int] = []


class EventUpdate(BaseModel):
    id: int
    name: str
    start_date: datetime
    end_date: datetime
    location: str
    description: str
    tags: List[str]
    hrs: int
    volunteers: List[int] = []


class EventOut(EventCreate):
    id: int


class EventRecord(
    namedtuple(
        "EventRecord",
        [
            "id",
            "name",
            "start_date",
            "end_date",
            "location",
            "description",
            "tags",
            "hrs",
            "volunteers",
            "account_id",
            "rating",
            "ratings",
        ],
    )
):
    pass


class EventRepository:
    def create(self, event: EventCreate, account_id: int) -> EventOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    print(db)
                    result = db.execute(
                        """
                        INSERT into events
                            (name, start_date, end_date, location, \
                                description, tags, hrs, volunteers, account_id)
                        VALUES
                            (%s,%s,%s,%s,%s,%s,%s,%s,%s)
                        RETURNING id;
                        """,
                        [
                            event.name,
                            event.start_date,
                            event.end_date,
                            event.location,
                            event.description,
                            event.tags,
                            event.hrs,
                            event.volunteers,
                            account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = event.dict()
                    return EventOut(id=id, **old_data)

        except Exception:
            return {"message": "event error!"}

    def get_all(self) -> List[Event]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT e.id, e.name, e.start_date, e.end_date, e.\
                            location, e.description, e.tags, e.hrs, e.\
                                volunteers, e.account_id, e.rating, e.ratings
                        FROM events e
                        ORDER BY e.start_date;
                        """,
                    )
                    records = db.fetchall()
                    return [
                        Event(
                            id=record.id,
                            name=record.name,
                            start_date=datetime.strptime(
                                record.start_date.strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            end_date=datetime.strptime(
                                record.end_date.strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            location=record.location,
                            description=record.description,
                            tags=record.tags,
                            hrs=record.hrs,
                            volunteers=record.volunteers,
                            account_id=record.account_id,
                            rating=record.rating,
                            ratings=record.ratings,
                        )
                        for record in map(EventRecord._make, records)
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get events for this account"}

    def get_by_account(self, account_id: int) -> List[Event]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT e.id, e.name, e.start_date, e.end_date, e.\
                            location, e.description, e.tags, e.hrs, e.\
                                volunteers, e.account_id, e.rating, e.ratings
                        FROM events e
                        JOIN accounts ON e.account_id = accounts.id
                        WHERE accounts.id = %s
                        ORDER BY e.start_date;
                        """,
                        [account_id],
                    )
                    records = db.fetchall()
                    return [
                        Event(
                            id=record[0],
                            name=record.name,
                            start_date=datetime.strptime(
                                record.start_date.strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            end_date=datetime.strptime(
                                record.end_date.strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            location=record.location,
                            description=record.description,
                            tags=record.tags,
                            hrs=record.hrs,
                            volunteers=record.volunteers,
                            account_id=record.account_id,
                            rating=record.rating,
                            ratings=record.ratings,
                        )
                        for record in map(EventRecord._make, records)
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get events for this account"}

    def get_joined_events_by_account(self, account_id: int) -> List[Event]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT e.id, e.name, e.start_date, e.end_date, e.\
                            location, e.description, e.tags, e.hrs, e.\
                                volunteers, e.account_id, e.rating, e.ratings\
                        FROM events e
                        JOIN accounts ON e.id = ANY(accounts.joined_events)
                        WHERE accounts.id = %s
                        ORDER BY e.start_date;
                        """,
                        [account_id],
                    )
                    records = db.fetchall()
                    return [
                        Event(
                            id=record[0],
                            name=record.name,
                            start_date=datetime.strptime(
                                record.start_date.strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            end_date=datetime.strptime(
                                record.end_date.strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            location=record.location,
                            description=record.description,
                            tags=record.tags,
                            hrs=record.hrs,
                            volunteers=record.volunteers,
                            account_id=record.account_id,
                            rating=record.rating,
                            ratings=record.ratings,
                        )
                        for record in map(EventRecord._make, records)
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get events for this account"}

    def update(self, event_id: int, event: EventUpdate) -> EventUpdate:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            UPDATE events SET\

                                name = %s,
                                start_date = %s,
                                end_date = %s,
                                location = %s,
                                description = %s,\

                                tags = %s,
                                hrs = %s,
                                volunteers = %s
                            WHERE id = %s;
                            """,
                        [
                            event.name,
                            event.start_date,
                            event.end_date,
                            event.location,
                            event.description,
                            event.tags,
                            event.hrs,
                            event.volunteers,
                            event_id,
                        ],
                    )
                    conn.commit()
                    old_data = event.dict()
                    return EventUpdate(**old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not update event."}

    def delete(self, event_id: int) -> Dict[str, str]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM events
                        WHERE id = %s;
                        """,
                        [event_id],
                    )
                    conn.commit()
                    return {
                        "message": f"Event with id {event_id} has been deleted"
                    }
        except Exception as e:
            print(e)
            return {"message": "Could not delete event."}

    def get_by_id(self, event_id: int) -> Union[Event, Dict[str, str]]:
        print("EVENTID:", event_id)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, start_date, end_date, location, \
                            description, tags, hrs, \
                            volunteers, account_id, rating, ratings
                        FROM events
                        WHERE id = %s;
                        """,
                        [event_id],
                    )
                    record = db.fetchone()
                    if record:
                        columns = [desc[0] for desc in db.description]
                        data = dict(zip(columns, record))
                        print("DATA", data)
                        return Event(
                            id=data["id"],
                            name=data["name"],
                            start_date=datetime.strptime(
                                data["start_date"].strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            end_date=datetime.strptime(
                                data["end_date"].strftime("%Y-%m-%d"),
                                "%Y-%m-%d",
                            ),
                            location=data["location"],
                            description=data["description"],
                            tags=data["tags"],
                            hrs=data["hrs"],
                            volunteers=data["volunteers"],
                            account_id=data["account_id"],
                            rating=data["rating"],
                            ratings=data["ratings"],
                        )
                    else:
                        return {
                            "message": f"No event found with id {event_id}"
                        }
        except Exception as e:
            print(e)
            return {"message": "Could not get event"}
