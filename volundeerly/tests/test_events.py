from fastapi.testclient import TestClient
from fastapi import status
from main import app
from queries.events import EventRepository, Event

client = TestClient(app)


class FakeEventQueries:

    def get_all(self):
        return []

    def get_by_id(self, event_id: int):
        return Event(
            id=event_id,
            name="Test Event",
            start_date="2023-03-10T15:49:07.127Z",
            end_date="2023-03-10T15:49:07.127Z",
            location="Test location",
            description="Test Description",
            tags=["tag1", "tag2"],
            hrs=2,
            volunteers=[1, 2, 3],
            account_id=2,
            rating=4.5,
            ratings=[5, 4, 4.5],
        )


def test_get_events():
    # Arrange
    app.dependency_overrides[EventRepository] = FakeEventQueries

    # Act
    res = client.get("/events")
    data = res.json()

    # Assert
    assert res.status_code == status.HTTP_200_OK
    assert data == []


def test_event_detail():
    # Arrange
    app.dependency_overrides[EventRepository] = FakeEventQueries

    # Act
    res = client.get("/events/detail/42")
    data = res.json()

    # Assert
    assert res.status_code == status.HTTP_200_OK
    assert data["id"] == 42


def test_event_volunteers():
    # Arrange
    app.dependency_overrides[EventRepository] = FakeEventQueries

    # Act
    res = client.get("/events/42/volunteers")
    data = res.json()

    # Assert
    assert res.status_code == status.HTTP_200_OK
    assert data == [1, 2, 3]
